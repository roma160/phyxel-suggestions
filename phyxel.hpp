// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.0
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// this is the main includable file of our incredible library
// everything is wrapped into the phx namespace
#pragma once
#include <cstdlib>
#include <cstdarg>
#include <string>
#include <vector>
#include <map>
#include <functional>
#include "phyxel_config.h"
#include "phyxel_maths.hpp"

// Phyxel classes
#if PHX_ENABLE_CUSTOMGRAVITY
    #define PHX_FORCE_DOWN 0
    #define PHX_FORCE_DOWNRIGHT 1
    #define PHX_FORCE_RIGHT 2
    #define PHX_FORCE_UPRIGHT 3
    #define PHX_FORCE_UP 4
    #define PHX_FORCE_UPLEFT 5
    #define PHX_FORCE_LEFT 6
    #define PHX_FORCE_DOWNLEFT 7
#endif
#if PHX_ENABLE_ELECTRICITY
    #define PHX_CHARGE_DIR_NODIR 0
    #define PHX_CHARGE_DIR_DOWN 1
    #define PHX_CHARGE_DIR_DOWNRIGHT 2
    #define PHX_CHARGE_DIR_RIGHT 3
    #define PHX_CHARGE_DIR_UPRIGHT 4
    #define PHX_CHARGE_DIR_UP 5
    #define PHX_CHARGE_DIR_UPLEFT 6
    #define PHX_CHARGE_DIR_LEFT 7
    #define PHX_CHARGE_DIR_DOWNLEFT 8
#endif
#if PHX_ENABLE_RIGIDBODIES // requires Box2D to be installed and linked (designed for v2.4.1)
    #include <box2d/box2d.h>
#endif
#include "Color.hpp"
#include "Material.hpp"
#include "MaterialsList.hpp"
#include "PhyxelData.hpp"
#include "Entity.hpp"
#include "Particle.hpp"
#include "Objects.hpp"
#include "Scene.hpp"
#include "SceneUpdates.hpp"
#include "EntityUtils.hpp"
#if PHX_ENABLE_CHEMISTRY
    #include "Chemistry.hpp"
#endif
#if PHX_ENABLE_RIGIDBODIES // requires Box2D to be installed and linked (designed for Box2D v2.4.1)
    #include "RigidBodyUtils.hpp"
#endif

// phyxel namespace variables
namespace phx {
    Scene scene;
}
