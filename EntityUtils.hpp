// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.0
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// this file has the definitions of some methods for the Entity system

#pragma once
#include "Entity.hpp"

namespace phx {

Entity::Entity(const Entity& e) {
    scene = e.scene;
    id = nextID;
    scene->logEntity(this);
    ++nextID;
}
Entity::Entity(Entity&& e) noexcept {
    scene = e.scene;
    id = nextID;
    scene->logEntity(this);
    ++nextID;
}
Entity& Entity::operator=(const Entity& e) {
    scene = e.scene;
    id = nextID;
    scene->logEntity(this);
    ++nextID;
    return (*this);
}
Entity& Entity::operator=(Entity&& e) noexcept {
    scene = e.scene;
    id = nextID;
    scene->logEntity(this);
    ++nextID;
    return (*this);
}
Entity::Entity(Scene* scene) {
    this->scene = scene;
    id = nextID;
    scene->logEntity(this);
    ++nextID;
}

Entity::~Entity() {
    scene->unlogEntity(this);
}

};