// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.0
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// do not delete any definitions from this file. the values can be changed freely unless told otherwise.

// sizes
#define PHX_SCENE_SIZE_X 128
#define PHX_SCENE_SIZE_Y 64
#define PHX_VIEW_SIZE_X 128
#define PHX_VIEW_SIZE_Y 64

// physics
/// set these to 0 to disable certain Phyxel's subsystems, as these features are computationally heavy
#define PHX_ENABLE_CHEMISTRY 1
#define PHX_ENABLE_CUSTOMGRAVITY 1
#define PHX_ENABLE_TEMPERATURE 1
#define PHX_ENABLE_ELECTRICITY 1 // only an imitation, not physically accurate
#define PHX_ENABLE_BURNING 1
#define PHX_ENABLE_RIGIDBODIES 1 // requires Box2D to be installed and linked (designed for Box2D v2.4.1). set to 0 if you don't have it

/// global numerical parameters (please use float literals, i.e. 1.0f instead of 1.0)
#define PHX_GAS_DIFF 400 // 0-1023
#define PHX_LIQ_DIFF 400 // 0-1023
#if PHX_ENABLE_TEMPERATURE
    #define PHX_MAX_TEMP 1500 // max temperature
    #define PHX_DEFAULT_TEMP 22.7f // default temperature is used to initialise scene.envTemperature
    #define PHX_MIN_TEMP -273.15f // min temperature
    #if PHX_ENABLE_BURNING
        #define PHX_BURNING_HEAT 30 // the amount of heat every burning phyxel produces each frame
    #endif
    #if PHX_ENABLE_ELECTRICITY
        #define PHX_ELECTRIC_HEAT 2 // the amount of heat every phyxel with electric current produces each frame
    #endif
#endif
#if PHX_ENABLE_RIGIDBODIES
    #define PHX_BOX2D_FREEFALL_ACCELERATION 15.0f
    #define PHX_METERS2PHYXELS 5.0f // Phyxel Engine tries to wrap Box2D as much as possible, so we try to only use phyxel-wise coordinates on the user side not to confuse the user
    #define PHX_PHYXELS2METERS 1.0f / PHX_METERS2PHYXELS // do not change, change PHX_METERS2PHYXELS instead
#endif
#define PHX_FREEFALL_ACCELERATION 4.0f // used for particles' movement
#if PHX_ENABLE_ELECTRICITY
    #define PHX_ELECTRIC_SPREAD 10
#endif
