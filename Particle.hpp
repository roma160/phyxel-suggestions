// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.0
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

#pragma once
#include "phyxel_config.h"
#include "Material.hpp"
#include <functional>

namespace phx {

class Scene; // defined in Scene.hpp and SceneUpdates.hpp

/// this struct is what our particles are. there are several pre-defined types of behaviour for particles. use custom updates adn destructors to expand it. the particle may escape the scene which will cause a segfault, make sure to delete such particles before they escape the scene or handle them properly
struct Particle: public Entity {
    // main fields
    MaterialObj material; // the material of the Particle, can be NULL if the particle is not "real"
    Color color;
    bool isAffectedByGravity;
    bool checkCollisions = true;
    bool isVirtual = false; // virtual particles have another, non-physical behaviour. in general, you should not create virtual particles, they are kept for technical use. virtual particles should always have a definite material that is NOT A SOLID. virtual particles are essentially particles that are trying to find a place to occupy.
    std::function<bool(Particle*, void*)> customUpdate = [](Particle*, void*){return false;}; // a functor to handle custom update routine. it's called prior to all the in-built particle physics. return true here to stop the automatic update of the particle.
    std::function<void(Particle*)> customDestructor = [](Particle*){return;}; // a functor to handle custom destructor routine

    // coordinates (in phyxels)
    float X;
    float Y;

    // velocity
    float Vx;
    float Vy;

    // burning
    #if PHX_ENABLE_BURNING
    bool isBurning;
    #endif

    // custom data (not used in any way by Phyxel, can be used in any way)
    void* customFields; // be careful with memory deallocation, put it into the customDestructor then

    Particle() = delete;
    Particle(MaterialObj material, Color color, float X, float Y,
        float Vx, float Vy,
        Scene* scene,
        bool isAffectedByGravity = true,
        bool checkCollisions = true
        #if PHX_ENABLE_BURNING
        ,bool isBurning = false
        #endif
    ): Entity(scene) {
        this->scene = scene;
        this->material = material;
        this->color = color;
        this->X = X;
        this->Y = Y;
        this->Vx = Vx;
        this->Vy = Vy;
        this->checkCollisions = checkCollisions;
        this->isAffectedByGravity = isAffectedByGravity;
        #if PHX_ENABLE_BURNING
        this->isBurning = isBurning;
        #endif
    }
};

}
