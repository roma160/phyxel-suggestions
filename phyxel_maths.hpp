// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.0
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// this file defines some of the additional mathematical tools used by phyxel

#pragma once
#include <cmath>
#include "phyxel_config.h"

namespace phx {

size_t inv_log_normal_CDF(int y) { // this function is an approximation of the discrete version of the inverse of the log normal distribution's CDF with certain parameters. 0 <= y <= 1023; and y is essentially a floating point number from [0;1] upscaled by 1023
    if (y <= 24) return 0;
    if (y <= 279) return 1;
    if (y <= 593) return 2;
    if (y <= 797) return 3;
    if (y <= 910) return 4;
    if (y <= 965) return 5;
    return 6;
}

template <typename T> void shuffle(T *array, size_t n) { // used to shuffle small arrays
    for (size_t i = 0; i < n-1; ++i) {
        size_t j = i + rand() % (n-i);
        std::swap(array[i], array[j]);
    }
}

}
