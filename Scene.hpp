// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.0
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// this declares (and partially defines the scene)

#pragma once
#include <cstdlib>
#include <vector>
#include <unordered_set>

#include "phyxel_config.h"
#if PHX_ENABLE_RIGIDBODIES // requires Box2D to be installed and linked (designed for v2.4.1)
    #include <box2d/box2d.h>
#endif
#include "Color.hpp"
#include "Material.hpp"
#if PHX_ENABLE_CHEMISTRY
    #include "Chemistry.hpp"
#endif
#include "PhyxelData.hpp"
#include "Particle.hpp"
#include "Objects.hpp"

namespace phx {

class Entity; // defined in "Entity.hpp"

/// a scene is everything we update each frame
class Scene {
    // these should be private and forbidden to use
    Scene(const Scene&) = delete;
    Scene(Scene&&) = delete;
    Scene& operator=(const Scene&) = delete;
    Scene& operator=(Scene&&) = delete;

    const uint16_t verticalLiqDiffusion = 1023 - PHX_LIQ_DIFF;
    const uint16_t verticalGasDiffusion = 1023 - PHX_GAS_DIFF;
    const uint16_t horizontalGasDiffusion = verticalGasDiffusion / 4;
    const float minDeltaMass = 1.1 * verticalGasDiffusion / (float) 1023;

    PhyxelData * phyxels;
    size_t sizeX;
    size_t sizeY;

    std::unordered_set<Entity*> existingEntities; // this includes instances, particles, rigid bodies. this set is only used to check if smth exists, for updates we use specialised vectors
    void logEntity(Entity* entity) {
        existingEntities.insert(entity);
    }
    void unlogEntity(Entity* entity) {
        existingEntities.erase(entity);
    }

    #if PHX_ENABLE_RIGIDBODIES
    void cleanupTmpBodies(size_t X, size_t Y); // remove covering rigids that are not used anymore
    #endif

    friend class Entity;
    friend class PhyxelAggregation;
public:
    std::vector<Particle*> particles; // DO NOT PUSH HERE, USE addParticle to create new ones
    std::vector<AbstractInstance*> instances; // contains any instances NOT including rigid objects (if PHX_ENABLE_RIGIDBODIES); DO NOT PUSH HERE, USE addInstance to create new ones
    void* customFields; // WARNING: clean it up yourself

    float particlesTimeStep = 0.5;

    #if PHX_ENABLE_TEMPERATURE
    float envTemperature=PHX_DEFAULT_TEMP;
    float envThermalDiff=0.00001;
    #endif
    #if PHX_ENABLE_RIGIDBODIES
    std::vector<RigidBody*> rigidBodies; // only contains references to rigid bodies; DO NOT PUSH HERE, USE addRigidBody to create new ones
    b2World* box2DLayer; // box2d world
    float box2DTimeStep = 1 / 60.f;
    int32 box2DVelocityIterations = 10;
    int32 box2DPositionIterations = 8;
    b2BodyDef box2DBodyDef;
    b2BodyDef box2DStaticBodyDef;
    b2FixtureDef box2DFixtureDef;
    b2FixtureDef box2DStaticFixtureDef;


    MaterialObj defaultMaterial = NULL; // this should be the only removable gas that rigid bodies will produce when moving in liquids
    #endif

    #if PHX_ENABLE_RIGIDBODIES
    class box2DContactListener : public b2ContactListener {
    public:
        std::function<void(b2Contact* contact, const b2Manifold* oldManifold)> preSolve = [](b2Contact* contact, const b2Manifold* oldManifold){return;}; // a functor to handle custom PreSolve
        std::function<void(b2Contact* contact, const b2ContactImpulse* impulse)> postSolve = [](b2Contact *contact, const b2ContactImpulse* impulse){return;}; // a functor to handle custom PostSolve
        std::function<void(b2Contact* contact)> beginContact = [](b2Contact* contact){return;}; // a functor to handle custom BeginContact
        std::function<void(b2Contact* contact)> endContact = [](b2Contact* contact){return;}; // a functor to handle custom EndContact

        void PreSolve(b2Contact* contact, const b2Manifold* oldManifold) {
            auto* fixData1 = reinterpret_cast<FixtureData*>(contact->GetFixtureA()->GetUserData().pointer);
            auto* fixData2 = reinterpret_cast<FixtureData*>(contact->GetFixtureB()->GetUserData().pointer);

            if ((fixData1->type==0 && reinterpret_cast<b2Fixture*>(fixData1->data)!=contact->GetFixtureB())
            || (fixData2->type==0 && reinterpret_cast<b2Fixture*>(fixData2->data)!=contact->GetFixtureA())) {
                contact->SetEnabled(false);
            }

            preSolve(contact, oldManifold);
        }
        virtual void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) {
            postSolve(contact, impulse);
        }
        virtual void BeginContact(b2Contact* contact) {
            beginContact(contact);
        }
        virtual void EndContact(b2Contact* contact) {
            endContact(contact);
        }
    };
    #endif

    // constructor
    Scene() {
        this->sizeX = PHX_SCENE_SIZE_X;
        this->sizeY = PHX_SCENE_SIZE_Y;

        #if PHX_ENABLE_RIGIDBODIES
        b2Vec2 gravity(0.0f, PHX_BOX2D_FREEFALL_ACCELERATION);
        box2DLayer = new b2World(gravity);
        box2DLayer->SetContactListener(new box2DContactListener());
        box2DBodyDef.type = b2_dynamicBody;
        box2DBodyDef.position.Set(0.0f, 0.0f);
        box2DBodyDef.linearVelocity.Set(0.0f, 0.0f);
        box2DStaticBodyDef.type = b2_staticBody;
        box2DStaticBodyDef.position.Set(0.0f, 0.0f);
        b2PolygonShape* boxShape = new b2PolygonShape();
        boxShape->SetAsBox(0.5*PHX_PHYXELS2METERS,0.5*PHX_PHYXELS2METERS);
        box2DStaticFixtureDef.shape = boxShape;
        #endif

        size_t tmp_size = sizeY*sizeX;

        phyxels = new PhyxelData[tmp_size];
    }

    // destructor
    ~Scene() {
        #if PHX_ENABLE_RIGIDBODIES
        for(size_t i = 0; i < rigidBodies.size(); ++i)
            delete rigidBodies[i];
        delete box2DLayer;
        #endif

        delete [] phyxels;
        for(size_t i = 0; i < particles.size(); ++i)
            delete particles[i];
        for(size_t i = 0; i < instances.size(); ++i)
            delete instances[i];
    }

    // getters for the most common properties. use scene(X,Y).property or scene[i].property if there's no getter provided
    const MaterialObj & getMaterial(size_t i) const {
        return phyxels[i].material;
    }
    const MaterialObj & getMaterial(size_t X, size_t Y) const {
        return phyxels[sizeX*Y+X].material;
    }
    const Color & getColor(size_t i) const {
        return phyxels[i].color;
    }
    const Color & getColor(size_t X, size_t Y) const {
        return phyxels[sizeX*Y+X].color;
    }
    #if PHX_ENABLE_TEMPERATURE
    float getTemperature(size_t i) const {
        return phyxels[i].temperature;
    }
    float getTemperature(size_t X, size_t Y) const {
        return phyxels[sizeX*Y+X].temperature;
    }
    #endif
    #if PHX_ENABLE_ELECTRICITY
    float getCharge(size_t i) const {
        return phyxels[i].charge;
    }
    float getCharge(size_t X, size_t Y) const {
        return phyxels[sizeX*Y+X].charge;
    }
    #endif
    #if PHX_ENABLE_RIGIDBODIES
    bool getIsRigidFree(size_t X, size_t Y) const {
        return phyxels[sizeX*Y+X].isRigidFree;
    }
    bool getIsRigidFree(size_t i) const {
        return phyxels[i].isRigidFree;
    }
    Color getRigidColor(size_t X, size_t Y) const {
        return phyxels[sizeX*Y+X].coveringRigid->color;
    }
    Color getRigidColor(size_t i) const {
        return phyxels[i].coveringRigid->color;
    }
    RigidBody* getCoveringRigid(size_t X, size_t Y) const {
        return phyxels[sizeX*Y+X].coveringRigid;
    }
    RigidBody* getCoveringRigid(size_t i) const {
        return phyxels[i].coveringRigid;
    }
    #endif
    const PhyxelData & operator()(size_t X, size_t Y) const { // double indexing
        return phyxels[sizeX*Y+X];
    }
    const PhyxelData & operator[](size_t idx) const { // single indexing
        return phyxels[idx];
    }
    size_t getSizeX() const {
        return sizeX;
    }
    size_t getSizeY() const {
        return sizeY;
    }
    #if PHX_ENABLE_CUSTOMGRAVITY
    uint8_t getForce(size_t i) const {
        return phyxels[i].forceDir;
    }
    uint8_t getForce(size_t X, size_t Y) const {
        return phyxels[sizeX*Y+X].forceDir;
    }
    #endif
    uint16_t getVerticalLiqDiffusion() {
        return verticalLiqDiffusion;
    }
    uint16_t getVerticalGasDiffusion() {
        return verticalGasDiffusion;
    }
    uint16_t getHorizontalGasDiffusion() {
        return horizontalGasDiffusion;
    }
    float getMinDeltaMass() {
        return minDeltaMass;
    }

    // setters for the most common properties. use scene(X,Y).property or scene[i].property if there's no setter provided
    void setMaterial(size_t X, size_t Y, MaterialObj m, int rnd=0) {
        setMaterial(Y*sizeX+X, m, rnd);
    }
    void setMaterial(size_t index, MaterialObj m, int rnd=0) {
        setMaterial(phyxels[index], m, rnd);
    }
    void setMaterial(PhyxelData& phyxel, MaterialObj m, int rnd=0) {
        phyxel.material = m;
        phyxel.color = m->colors[rnd%(m->colors.size())];
        #if PHX_ENABLE_RIGIDBODIES
        for (auto* body : phyxel.box2DTmpBodies) {
            auto* fixData = reinterpret_cast<FixtureData*>(body->GetFixtureList()->GetUserData().pointer);
            delete fixData;
            box2DLayer->DestroyBody(body);
        }
        phyxel.box2DTmpBodies.clear();
        phyxel.deleteTmpBody.clear();
        #endif
    }
    void setColor(size_t X, size_t Y, Color val) {
        phyxels[sizeX*Y+X].color = val;
    }
    void setColor(size_t i, Color val) {
        phyxels[i].color = val;
    }
    PhyxelData & operator()(size_t X, size_t Y) { // double indexing
        return phyxels[sizeX*Y+X];
    }
    PhyxelData & operator[](size_t idx) { // single indexing
        return phyxels[idx];
    }
    #if PHX_ENABLE_TEMPERATURE
    void setTemperature(size_t X, size_t Y, float tau){
        phyxels[sizeX*Y+X].temperature = tau;
    }
    void setTemperature(size_t i, float tau){
        phyxels[i].temperature = tau;
    }
    void addTemperature(size_t X, size_t Y, float delta_tau) {
        phyxels[sizeX*Y+X].temperature += delta_tau;
    }
    void addTemperature(size_t i, float delta_tau) {
        phyxels[i].temperature += delta_tau;
    }
    #endif
    #if PHX_ENABLE_ELECTRICITY
    void setCharge(size_t X, size_t Y, float dQ) {
        size_t i = sizeX*Y+X;
        phyxels[i].charge = dQ;
        phyxels[i].isTurning = true;
    }
    void setCharge(size_t i, float dQ) {
        phyxels[i].charge = dQ;
        phyxels[i].isTurning = true;
    }
    void addCharge(size_t X, size_t Y, float dQ) {
        phyxels[sizeX*Y+X].charge += dQ;
    }
    void addCharge(size_t i, float dQ) {
        phyxels[i].charge += dQ;
    }
    #endif
    #if PHX_ENABLE_CUSTOMGRAVITY
    void setForce(size_t X, size_t Y, uint8_t forceDir) {
        phyxels[sizeX*Y+X].forceDir = forceDir;
    }
    void setForce(size_t i, uint8_t forceDir) {
        phyxels[i].forceDir = forceDir;
    }
    #endif

    // adding entities to the scene
    Particle* addParticle(
        MaterialObj material, Color color,
        float X, float Y,
        float Vx, float Vy,
        bool isAffectedByGravity = true,
        bool checkCollisions = true
        #if PHX_ENABLE_BURNING
        ,bool isBurning = false
        #endif
    ) {
        Particle* p = new Particle(material, color, X, Y, Vx, Vy, this, isAffectedByGravity, checkCollisions
        #if PHX_ENABLE_BURNING
        ,isBurning
        #endif
        );
        particles.push_back(p);
        return p;
    }
    void addVirtualParticle(
        MaterialObj material, Color color,
        float X, float Y,
        float Vx, float Vy
    ) {
        auto p = addParticle(material, color, X, Y, Vx, Vy);
        p->isVirtual = true;
    }
    AbstractInstance* addInstance(AbstractInstance* i) { // should not be used for rigid bodies (in case you have PHX_ENABLE_RIGIDBODIES), use addRigidBody for them. only use this to add some other instances
        instances.push_back(i);
        return i;
    }
    #if PHX_ENABLE_RIGIDBODIES // (coordinates are expected in phyxels and not Box2D's metre units)
    RigidBody* addRigidBody(float x0, float y0, float _sizeX, float _sizeY, MaterialObj material) {
        return addRigidBody<void>(x0, y0, _sizeX, _sizeY, material, nullptr);
    }
    template<typename T> RigidBody* addRigidBody(float x0, float y0, float _sizeX, float _sizeY, MaterialObj material, T* userData, int userDataType=1) { // this can be used to add userdata to the inner box2d body of phyxel rigid bodies
        box2DBodyDef.position.Set(x0*PHX_PHYXELS2METERS, y0*PHX_PHYXELS2METERS);
        b2Body* body = box2DLayer->CreateBody(&box2DBodyDef);
        RigidBody* rigidBody = new RigidBody(this, box2DLayer, body, _sizeX, _sizeY, material);
        #if PHX_ENABLE_TEMPERATURE
        rigidBody->temperature = envTemperature;
        #endif
        b2PolygonShape dynamicBox;
        dynamicBox.SetAsBox(_sizeX*0.5*PHX_PHYXELS2METERS, _sizeY*0.5*PHX_PHYXELS2METERS);
        box2DFixtureDef.shape = &dynamicBox;
        box2DFixtureDef.density = 1.0f;
        box2DFixtureDef.friction = 0.3f;
        if (!userDataType)  // userDataType=0 is reserved for tmp bodies.
            userDataType = 1;
        box2DFixtureDef.userData.pointer = reinterpret_cast<uintptr_t>(new FixtureData(userData, userDataType));
        body->CreateFixture(&box2DFixtureDef);
        rigidBody->gravForce = PHX_BOX2D_FREEFALL_ACCELERATION * body->GetMass();
        rigidBodies.push_back(rigidBody);
        return rigidBody;
    }
    #endif

    // swaps
    void swap(size_t x1, size_t y1, size_t x2, size_t y2, bool loopDir) { // swap phyxels (only the necessary parts) /// THIS FUNCTION ISN'T SYMMETRIC
        auto offset2 = sizeX*y2+x2;
        auto offset1 = sizeX*y1+x1;

        phyxels[offset1].lockMask = 1;
        if (phyxels[offset2].lockMask & 1) return;
        swapNoLock(offset1, offset2, loopDir);

        if(loopDir == (offset1 < offset2))
            phyxels[offset2].lockMask = 3;
        else
            phyxels[offset2].lockMask = 1;
    }
    void swapNoLock(size_t offset1, size_t offset2, bool loopDir) { // swap phyxels without locking them
        std::swap(phyxels[offset1].material, phyxels[offset2].material);
        std::swap(phyxels[offset1].color, phyxels[offset2].color);

        #if PHX_ENABLE_RIGIDBODIES
        for (auto* body : phyxels[offset1].box2DTmpBodies) {
            auto* fixData = reinterpret_cast<FixtureData*>(body->GetFixtureList()->GetUserData().pointer);
            delete fixData;
            box2DLayer->DestroyBody(body);
        }
        phyxels[offset1].box2DTmpBodies.clear();
        phyxels[offset1].deleteTmpBody.clear();

        for (auto* body : phyxels[offset2].box2DTmpBodies) {
            auto* fixData = reinterpret_cast<FixtureData*>(body->GetFixtureList()->GetUserData().pointer);
            box2DLayer->DestroyBody(body);
        }
        phyxels[offset2].box2DTmpBodies.clear();
        phyxels[offset2].deleteTmpBody.clear();
        #endif
        #if PHX_ENABLE_TEMPERATURE
        std::swap(phyxels[offset1].temperature, phyxels[offset2].temperature);
        #endif
        // no swap for electricity
        #if PHX_ENABLE_BURNING
        std::swap(phyxels[offset1].burningStage, phyxels[offset2].burningStage);
        std::swap(phyxels[offset1].burningTimer, phyxels[offset2].burningTimer);
        #endif
        std::swap(phyxels[offset1].customFields, phyxels[offset2].customFields);
    }
    void swapNoLock(size_t X1, size_t Y1, size_t X2, size_t Y2, bool loopDir) {
        swapNoLock(sizeX * Y1 + X1, sizeX * Y2 + X2, loopDir);
    }

    // fill in the scene (call it after init)
    void fill(MaterialObj defaultMaterial) {
        for(size_t i=0; i<sizeX*sizeY; ++i)
            setMaterial(i, defaultMaterial, 0);
    }
    void fillFrame(MaterialObj frameMaterial) {
        size_t tmp = (sizeX*(sizeY-1));
        for(size_t i = 0; i < sizeX; ++i) {
            phyxels[i].material = frameMaterial;
            phyxels[i + tmp].material = frameMaterial;
        }
        for(size_t i = 1; i < sizeY-1; ++i) {
            phyxels[i*sizeX].material = frameMaterial;
            phyxels[(i+1)*sizeX - 1].material = frameMaterial;
        }
    }
    #if PHX_ENABLE_TEMPERATURE
    void fillTemperature(float tau) {
        for(size_t i=0; i<sizeX*sizeY; ++i)
            phyxels[i].temperature = tau;
    }
    #endif
    #if PHX_ENABLE_CUSTOMGRAVITY
    void alignForce(uint8_t forceDir) {
        for(size_t i=0; i<sizeX*sizeY; ++i)
            phyxels[i].forceDir = forceDir;
    }
    #endif

    // check if something exists. does not check if it is the same entity (no id check), so basically only checks if the pointer is a valid entity pointer and nothing else 
    bool exists(Entity* entity) {
        return (existingEntities.find(entity) != existingEntities.end());
    }

    // updates (defined in "SceneUpdates.hpp")
    void updateAll( // updates everything, can be called with 0 arguments like scene.updateAll(), they all default to empty lists
        void* custom_arguments_particles,
        void* custom_arguments_instances
        #if PHX_ENABLE_RIGIDBODIES
        ,void* custom_arguments_rigids
        #endif
    );
    void updateAllPhyxels(); // combined (temperature + chemistry + burning + electricity + movement) & balanced (bidirectional loop) update for every phyxel in the scene
    #if PHX_ENABLE_TEMPERATURE
    void updateTemperature(size_t X, size_t Y); // only updates temperatures
    #endif
    #if PHX_ENABLE_ELECTRICITY
    void updateElectricity(size_t X, size_t Y); // only updates electricity
    #endif
    #if PHX_ENABLE_CHEMISTRY
    bool updateChemistry(size_t X, size_t Y); // only updates chemistry
    #endif
    #if PHX_ENABLE_BURNING
    void updateBurning(size_t X, size_t Y); // only updates fire
    #endif
    void updateInstances(void* custom_arguments); // only updates other instances (not including rigid bodies)
    bool updatePhyxelMovement(size_t X, size_t Y, bool loopDir=true); // update phyxels' movement
    void updateParticles(void* custom_arguments); // only updates particles
    #if PHX_ENABLE_RIGIDBODIES
    void rigidBodyFrameLoop(RigidBody* body);
    void updateRigidBodies(void* custom_arguments); // only updates rigid bodies
    #endif
};
}
