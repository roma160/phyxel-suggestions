// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.0
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// this file contains the definition of our PhyxelData class, which is what the scene consists of

#pragma once
#include "phyxel_config.h"
#include "MaterialsList.hpp"

#if PHX_ENABLE_RIGIDBODIES // requires Box2D to be installed and linked (designed for v2.4.1)
    #include <box2d/box2d.h>
    #include "Objects.hpp"
#endif

namespace phx {

class Scene; // defined in "Scene.hpp"
#if PHX_ENABLE_RIGIDBODIES
class RigidBody;
#endif

class PhyxelData {
  // electricity
  #if PHX_ENABLE_ELECTRICITY
  float chargeLifetime = 0; // cumulative resistance gained, when >1 then death
  bool isTurning = false;
  #endif

  // rigid bodies
  #if PHX_ENABLE_RIGIDBODIES
  std::vector<b2Body*> box2DTmpBodies; // "rigid bodies" attached to this phyxel so that real rigids don't fall through it
  bool isRigidFree; // whether the phyxel is occupied by a rigid body or not
  RigidBody* coveringRigid; // the rigid body that covers this phyxel (or NULL if isRigidFree is true)
  std::vector<bool> deleteTmpBody; // is true when the attached tmp body is not in use anymore and should be deleted
  #if PHX_ENABLE_ELECTRICITY
  bool isChargeFromBody; // whether the charge is related to the rigid body or to the phyxel itself
  #endif
  #endif

  // friends
  friend class Scene;
  #if PHX_ENABLE_RIGIDBODIES
  friend class RigidBody;
  #endif
public:
  // main fields
  MaterialObj material; // the material of the phyxel
  Color color;
  uint8_t lockMask; // used to lock phyxels during updates, changing not recommended

  // temperature
  #if PHX_ENABLE_TEMPERATURE
  float temperature; // welp, that's just the temperature
  #endif

  // electricity
  #if PHX_ENABLE_ELECTRICITY
  float charge = 0; // welp, that's just the charge
  uint8_t chargeDir = PHX_CHARGE_DIR_NODIR; // which direction is forbidden to go
  #endif

  // custom force fields
  #if PHX_ENABLE_CUSTOMGRAVITY
  uint8_t forceDir;
  #endif

  // burning
  #if PHX_ENABLE_BURNING
  uint8_t burningStage = 0;
  time_t burningTimer; // technical
  #endif

  // rigid bodies
  #if PHX_ENABLE_RIGIDBODIES
  RigidBody* getCoveringRigid() {
    return coveringRigid;
  }
  bool getIsRigidFree() {
    return isRigidFree;
  }
  #if PHX_ENABLE_ELECTRICITY
  bool getIsChargeFromBody() {
    return isChargeFromBody;
  }
  #endif
  #endif

  // custom data (not used by Phyxel, can be used in any way for your custom fields, be careful with memory deallocation though)
  void* customFields;

  PhyxelData():
    PhyxelData(NULL, Color{0,0,0,0},
    #if PHX_ENABLE_TEMPERATURE
    PHX_MIN_TEMP,
    #endif
    #if PHX_ENABLE_CUSTOMGRAVITY
    PHX_FORCE_DOWN,
    #endif
    0) {}

  PhyxelData(MaterialObj material, Color color,
    #if PHX_ENABLE_TEMPERATURE
    float temperature,
    #endif
    #if PHX_ENABLE_CUSTOMGRAVITY
    uint8_t forceDir,
    #endif
    uint8_t lockMask) {
      this->material = material;
      this->color = color;
      #if PHX_ENABLE_TEMPERATURE
      this->temperature = temperature;
      #endif
      #if PHX_ENABLE_CUSTOMGRAVITY
      this->forceDir = forceDir;
      #endif
      this->lockMask = lockMask;
      #if PHX_ENABLE_RIGIDBODIES
      box2DTmpBodies = std::vector<b2Body*>();
      deleteTmpBody = std::vector<bool>();
      isRigidFree = true;
      coveringRigid = NULL;
      #if PHX_ENABLE_ELECTRICITY
      isChargeFromBody = false;
      #endif
      #endif
    }

    #if PHX_ENABLE_BURNING
    void tryToBurn(uint8_t isBurning=true) {
        burningStage = std::max((uint8_t)(isBurning&&(material->isFlamable)), burningStage);
    }
    #endif
};

}
