// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.0
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// this file contains the definitions for the following classes that are somewhat related: Instance, RigidBody, FixtureData (technical), RigidJoint

#pragma once
#include <cstdlib>
#include <vector>
#include <cmath>
#include <algorithm>

#include "phyxel_config.h"
#include "Entity.hpp"
#if PHX_ENABLE_RIGIDBODIES // requires Box2D to be installed and linked (designed for v2.4.1)
    #include <box2d/box2d.h>
    #include "Color.hpp"
#endif

namespace phx {

class Scene; // defined in "Scene.hpp"

/// you can (and should) inherit from this class to define your own non-physical objects/instances that are going to be put onto the scene. for the physical ones use our rigid body system
class AbstractInstance: public Entity {
public:
    std::function<void(AbstractInstance*, void*)> customUpdate = [](AbstractInstance*, void*){}; // a functor to handle custom update routine

    AbstractInstance(Scene* scene): Entity(scene) {};
    virtual ~AbstractInstance() {};

    virtual float getX() = 0;
    virtual float getY() = 0;

    friend class Scene;
};

#if PHX_ENABLE_RIGIDBODIES // only load this part if rigid bodies are turned on

class RigidBody; // declared later in the file

/// technical structure
struct FixtureData {
    uintptr_t data;
    int type;
    template <typename T>
    FixtureData(T* data, int type): data(reinterpret_cast<uintptr_t>(data)), type(type) {}
};

/// this allows grouping different rigid bodies into a single physical structure. basically a safe wrapper of Box2D's box2DJoint into the Phyxel Engine. 
// use linkRigidBodies (defined in "RigidBodyUtils.hpp") for the sake of safety and stability, it has more chance to be included in the future
class RigidJoint {
    b2World* box2DLayer;
public:
    b2Joint* box2DJoint;
    RigidBody* firstBody;
    RigidBody* secondBody;

    RigidJoint(b2World* box2DLayer, RigidBody* Body1, RigidBody* Body2); // defined in "RigidBodyUtils.hpp"

    ~RigidJoint() {
        box2DLayer->DestroyJoint(box2DJoint);
    }
};

/// Phyxel engine's RigidBody. it's based on Box2D's bodies.
class RigidBody: public AbstractInstance {
    // do NOT explicitly delete objects from the scene, just set obj.toDelete = true instead. this will then call the destructor when the object gets updated. otherwise you'll DEFINITELY have problems and the object will not be removed fully.
    // sizes in phyxels
    float sizeX;
    float sizeY;

protected:
    b2World* box2DLayer;
    // coordinates of radius vectors of body's vertices
    float vX[4];
    float vY[4];

    // coordinates of vectors representing body's edges
    float edgeX[4];
    float edgeY[4];

    // sin and cos of body's rotation angle divided by 2 (c = cos(x)/2)
    float s;
    float c;

    // gravity force
    double gravForce;

    virtual ~RigidBody(); // defined in RigidBodyUtils.hpp
public:
    std::vector<std::pair<size_t, size_t>> coveredPhyxels;
    std::vector<std::pair<size_t, size_t>> framePhyxels;

    // material
    MaterialObj material;
    // background material
    float backgroundMaterialMass=0;
    // temperature
    #if PHX_ENABLE_TEMPERATURE
    float temperature = 22.7f;
    #endif
    // burning
    #if PHX_ENABLE_BURNING
    uint8_t burningStage = 0;
    time_t burningTimer; // technical
    #endif

    // display (you may ignore that and draw a texture instead)
    Color color;

    // related body in the world
    b2Body* box2DBody;

    // attached joints
    std::vector<RigidJoint*> joints;



    // constr
    RigidBody(Scene* scene, b2World* box2DLayer, b2Body* body, float sizeX, float sizeY, MaterialObj material); // defined in "RigidBodyUtils.hpp" // it's not recommended to create objects manually, add them to the scene using Scene::addRigidBody instead 

    // getters (coordinates are returned in phyxels and not Box2D's metre units)
    // common getters. if there are none provided for what you need, try using it with the box2DBody directly, but be careful with units
    virtual float getX() { 
        return box2DBody->GetPosition().x * PHX_METERS2PHYXELS;
    }
    virtual float getY() {
        return box2DBody->GetPosition().y * PHX_METERS2PHYXELS;
    }
    virtual std::pair<float, float> getCoords() {
        b2Vec2 b2d_coords = box2DBody->GetPosition();
        std::pair<float, float> phx_coords {
            b2d_coords.x * PHX_METERS2PHYXELS,
            b2d_coords.y * PHX_METERS2PHYXELS
        };
        return phx_coords;
    }
    virtual float getSizeX() {
        return sizeX;
    }
    virtual float getSizeY() {
        return sizeY;
    }
    virtual float getAngle() { // get angle in radians
        return box2DBody->GetAngle();
    }
    virtual float getAngularVelocity() { // get angular velocity in radians/seconds
        return box2DBody->GetAngularVelocity();
    }
    virtual std::pair<float, float> getLinearVelocity() {
        b2Vec2 b2d_coords = box2DBody->GetLinearVelocity();
        std::pair<float, float> phx_coords {
            b2d_coords.x,
            b2d_coords.y
        };
        return phx_coords;
    }

    // setters (coordinates are expected in phyxels and not Box2D's metre units)
    // common setters. if there are none provided for what you need, try using it with the box2DBody directly, but be careful with units
    virtual void setCoords(float X, float Y) {
        box2DBody->SetTransform(
            b2Vec2{static_cast<float>(X * PHX_PHYXELS2METERS), 
            static_cast<float>(Y * PHX_PHYXELS2METERS)}, 
            box2DBody->GetAngle()
        );
    }
    virtual void setAngle(float phi) { // in radians
        box2DBody->SetTransform(box2DBody->GetPosition(), phi);
    }
    virtual void recalculate(); // defined in "RigidBodyUtils.hpp" // shouldn't really be called by the user, it's regularly called automatically

    // put the body into the phyxel layer
    virtual void phyxelate(); // defined in "RigidBodyUtils.hpp"

    // update burning
    #if PHX_ENABLE_BURNING
    virtual void updateBurning(); // defined in "RigidBodyUtils.hpp"
    #endif

    friend class Scene;
    friend RigidJoint* linkRigidBodies(RigidBody* b1, RigidBody* b2); // defined in "RigidBodyUtils.hpp"
};

#endif

}
