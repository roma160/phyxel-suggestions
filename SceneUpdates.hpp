// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.0
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// this file contains the definitions for all the update methods of the Scene class

#pragma once

#include "Scene.hpp"
#include "phyxel_maths.hpp"
#include <cmath>

namespace phx {

#if PHX_ENABLE_TEMPERATURE
void Scene::updateTemperature(size_t X, size_t Y) { // only updates temperatures
    int rnd = rand();
    #if PHX_ENABLE_RIGIDBODIES // a sofisticated yet still slow temperature update that includes rigid bodies
        int8_t binrand = rnd & 3;
        size_t idx_cur = sizeX*Y+X;
        float* tau_right;
        float therm_diff_right;
        if (phyxels[idx_cur+1].isRigidFree) {
            tau_right = &phyxels[idx_cur+1].temperature;
            therm_diff_right = phyxels[idx_cur+1].material->thermalDiff;
        } else {
            tau_right = &phyxels[idx_cur+1].coveringRigid->temperature;
            therm_diff_right = phyxels[idx_cur+1].coveringRigid->material->thermalDiff;
        }
        MaterialObj* cur = NULL;
        float therm_diff_cur;
        float* tau_cur = NULL;
        if (phyxels[idx_cur].coveringRigid) {
            cur = &(phyxels[idx_cur].coveringRigid->material); // current phyxel that we're looking at
            tau_cur = &phyxels[idx_cur].coveringRigid->temperature; // current temperature
        } else {
            cur = &(phyxels[idx_cur].material); // current phyxel that we're looking at
            tau_cur = &phyxels[idx_cur].temperature; // current temperature
        }
        float tau_initial = *tau_cur; // initial temperature
        // This line was deleted at 74788fb117a588cebfea29a9d2bd136d9cc97e25
        // Added back to fix runtime error "Variable therm_diff_cur is being used without being initialized".
        therm_diff_cur = (*cur)->thermalDiff;

        float delta_tau = (tau_initial - *tau_right)*((binrand&1) ? therm_diff_cur : therm_diff_right);
        *tau_cur -= delta_tau;
        *tau_right += delta_tau;

        size_t idx_lower = idx_cur+sizeX;
        float* tau_lower;
        float therm_diff_lower;
        if (phyxels[idx_lower].isRigidFree) {
            tau_lower = &phyxels[idx_lower].temperature;
            therm_diff_lower = phyxels[idx_lower].material->thermalDiff;
        } else {
            tau_lower = &phyxels[idx_lower].coveringRigid->temperature;
            therm_diff_lower = phyxels[idx_lower].coveringRigid->material->thermalDiff;
        }
        delta_tau = (tau_initial - *tau_lower)*(binrand&2 ? therm_diff_cur : therm_diff_lower);
        *tau_cur -= delta_tau;
        *tau_lower += delta_tau;

        *tau_cur -= (tau_initial - envTemperature)*envThermalDiff;

        if (*tau_cur >= (*cur)->stateMaxTemp) {
            if (phyxels[idx_cur].isRigidFree)
                setMaterial(idx_cur, (*cur)->higherTempState, rnd);
            else {
                *cur = (*cur)->higherTempState;
                phyxels[idx_cur].coveringRigid->phyxelate();
            }
        } else if (*tau_cur <= (*cur)->stateMinTemp) {
            setMaterial(idx_cur, (*cur)->lowerTempState, rnd);
            if (phyxels[idx_cur].isRigidFree)
                setMaterial(idx_cur, (*cur)->lowerTempState, rnd);
            else {
                *cur = (*cur)->lowerTempState;
                phyxels[idx_cur].coveringRigid->phyxelate();
            }
        }
    #else // simple old temperature update without rigid bodies
        int8_t binrand = rnd % 2;
        size_t idx_cur = sizeX*Y+X;
        auto cur = phyxels[idx_cur].material; // current phyxel that we're looking at
        float tau_cur = phyxels[idx_cur].temperature; // current temperature
        float delta_tau = (tau_cur - phyxels[idx_cur+1].temperature)*(phyxels[idx_cur+binrand].material->thermalDiff);
        phyxels[idx_cur].temperature -= delta_tau;
        phyxels[idx_cur+1].temperature += delta_tau;
        size_t idx_lower = sizeX*(Y+1)+X;
        delta_tau = (tau_cur - phyxels[idx_lower].temperature)*(phyxels[idx_cur+binrand*sizeX].material->thermalDiff);
        phyxels[idx_cur].temperature -= delta_tau;
        phyxels[idx_lower].temperature += delta_tau;

        phyxels[idx_cur].temperature -= (tau_cur - envTemperature)*envThermalDiff;

        tau_cur = phyxels[idx_cur].temperature;
        if (tau_cur >= cur->stateMaxTemp) {
            cur = cur->higherTempState;
            setMaterial(idx_cur, cur, rnd);
        } else if (tau_cur <= cur->stateMinTemp) {
            cur = cur->lowerTempState;
            setMaterial(idx_cur, cur, rnd);
        }
    #endif // PHX_ENABLE_RIGIDBODIES
}
#endif // PHX_ENABLE_TEMPERATURE

#if PHX_ENABLE_ELECTRICITY
void Scene::updateElectricity(size_t X, size_t Y) { // only updates electricity
    static int8_t Vx[] = {0, 1, 1, 1, 0, -1, -1, -1};
    static int8_t Vy[] = {1, 1, 0, -1, -1, -1, 0, 1};
    static uint8_t idx_order[] = {0, 2, 4, 6, 1, 3, 5, 7};

    auto cur_phx = phyxels+sizeX*Y+X;
    #if PHX_ENABLE_RIGIDBODIES
    auto phx_body = cur_phx->coveringRigid;
    #endif
    auto cur_mat = cur_phx->material;

    if (cur_phx->charge != 0) {
        // update lifetime & kill charges
        #if PHX_ENABLE_RIGIDBODIES
        cur_phx->chargeLifetime += ((/*cur_phx->isChargeFromBody && */phx_body) ? phx_body->material->resistance : cur_mat->resistance);
        #else
        cur_phx->chargeLifetime += cur_mat->resistance;
        #endif
        if (cur_phx->chargeLifetime >= 1.) {
            cur_phx->charge = 0;
            cur_phx->chargeLifetime = 0;
            cur_phx->isTurning = false;
            #if PHX_ENABLE_RIGIDBODIES
            cur_phx->isChargeFromBody = false;
            #endif
            return;
        }

        if (cur_phx->isTurning) {
            // pick a new dir that is not backwards
            phx::shuffle<uint8_t>(idx_order, 4);
            phx::shuffle<uint8_t>(idx_order + 4, 5);
            long long idx_shift = 0;
            uint8_t dir = 0;
            uint8_t forbidden_dir = (cur_phx->chargeDir != 0)*((cur_phx->chargeDir+3)&7+1);
            for (uint8_t i = 0; i < 8; ++i) {
                if (idx_order[i]+1 != forbidden_dir) {
                    idx_shift = Vx[idx_order[i]]+sizeX*Vy[idx_order[i]];
                    dir = idx_order[i]+1;
                } else {
                    idx_shift = Vx[cur_phx->chargeDir-1]+sizeX*Vy[cur_phx->chargeDir-1];
                    dir = cur_phx->chargeDir;
                }
                auto try_phx = (cur_phx+idx_shift);
                auto try_mat = try_phx->material;
                #if PHX_ENABLE_RIGIDBODIES
                if (!(try_phx->isRigidFree) && try_phx->coveringRigid->material->resistance < 1 && try_phx->charge==0) {
                    try_phx->isChargeFromBody = true;
                    break;
                } else
                #endif
                if (try_mat->resistance < 1 && try_phx->charge==0) break;
            }

            // roll-out loop
            auto loop_phx = cur_phx;
            for (size_t _ = 0; _ < PHX_ELECTRIC_SPREAD; ++_) {
                auto loop_next_phx = loop_phx + idx_shift;
                #if PHX_ENABLE_RIGIDBODIES
                if (!(loop_next_phx->isRigidFree) && loop_next_phx->coveringRigid->material->resistance < 1) {
                    loop_phx->isChargeFromBody = true;
                    loop_phx = loop_next_phx;
                }
                else
                #endif
                if (loop_next_phx->material->resistance < 1) loop_phx = loop_next_phx;
                else {
                    bool free_found = false;
                    forbidden_dir = (dir != 0)*((dir+3)&7+1);
                    for (uint8_t i = 0; i < 8; ++i) {
                        if (idx_order[i]+1 != forbidden_dir) {
                            idx_shift = Vx[idx_order[i]]+sizeX*Vy[idx_order[i]];
                            dir = idx_order[i]+1;
                        } else continue;
                        auto try_phx = (cur_phx+idx_shift);
                        auto try_mat = try_phx->material;
                        #if PHX_ENABLE_RIGIDBODIES
                        if (!(try_phx->isRigidFree) && try_phx->coveringRigid->material->resistance < 1 && try_phx->charge==0) {
                            try_phx->isChargeFromBody = true;
                            free_found = true;
                            break;
                        } else
                        #endif
                        if (try_mat->resistance < 1 && try_phx->charge==0) {
                            free_found = true;
                            break;
                        }
                    }
                    if (!free_found) break;
                }

                #if PHX_ENABLE_TEMPERATURE
                #if PHX_ENABLE_RIGIDBODIES
                if (!(loop_phx->isRigidFree)) loop_phx->coveringRigid->temperature += PHX_ELECTRIC_HEAT * cur_phx->charge;
                else
                #endif
                loop_phx->temperature += PHX_ELECTRIC_HEAT * cur_phx->charge;
                #endif
                loop_phx->charge = cur_phx->charge;
                loop_phx->chargeLifetime = cur_phx->chargeLifetime;
                loop_phx->chargeDir = dir;
            }
            loop_phx->isTurning = true;

            //if (idx_order[0]<4) cur_phx->isTurning = false;
        }
    }
}
#endif // PHX_ENABLE_ELECTRICITY

#if PHX_ENABLE_CHEMISTRY
bool Scene::updateChemistry(size_t X, size_t Y) { // only updates chemistry
    size_t index_cur = sizeX*Y+X;
    auto cur_material = phyxels[index_cur].material; // the material of the current phyxel we're looking at
    auto table = cur_material->chemTable; // can be NULL
    #if PHX_ENABLE_TEMPERATURE
    float tau_cur = phyxels[index_cur].temperature; // current temperature
    #endif

    if (
        table
        #if PHX_ENABLE_RIGIDBODIES
        || (phyxels[index_cur].coveringRigid && !(phyxels[index_cur].coveringRigid->toDelete) && phyxels[index_cur].coveringRigid->material->chemTable)
        #endif
    ) {
        int rnd = rand() & 8191;
        int8_t rnd_axis = rnd % 2, dir = (rand()%2)*2 - 1;

        size_t index[] = {
            (rnd_axis*sizeX + !rnd_axis)*dir,
            index_cur,
            ((!rnd_axis)*sizeX + rnd_axis)*dir,
            index_cur
        };
        index[1] -= index[0];
        index[0] += index_cur;
        index[3] -= index[2];
        index[2] += index_cur;

        #if PHX_ENABLE_RIGIDBODIES
        bool from_rigid = false; // whether the self in the reaction comes from the phyxel or the rigid that covers it
        for (uint8_t phyx_or_rigid = 0; phyx_or_rigid < 2; ++phyx_or_rigid) { // this shows where the chemTable comes from 0 - phyxel check, 1 - rigid check
            if (phyx_or_rigid) {
                // cur_material = phyxels[index_cur].coveringRigid->material;
                if (phyxels[index_cur].isRigidFree || phyxels[index_cur].coveringRigid->toDelete) continue;
                table = phyxels[index_cur].coveringRigid->material->chemTable;
                from_rigid = true;
                #if PHX_ENABLE_TEMPERATURE
                tau_cur = phyxels[index_cur].coveringRigid->temperature;
                #endif
            }
            if (!table) continue;
        #endif
        for (uint8_t i = 0; i < 4; ++i) {
            #if PHX_ENABLE_RIGIDBODIES
            bool for_rigid = (phyxels[index[i]].coveringRigid && !(phyxels[index[i]].coveringRigid->toDelete)); // is reaction caused by the material of the attached rigid body or no (meaning whether the Other component of the reaction came from the material of the phyxel or the rigid body above it) // FIXME: this check repeats the check above
            if (from_rigid && for_rigid && (phyxels[index[i]].coveringRigid == phyxels[index_cur].coveringRigid)) {
                continue; // we're not checking interactions of the body with its own phyxels
            }
            auto found = (for_rigid ? table->find(phyxels[index[i]].coveringRigid->material) : table->find(phyxels[index[i]].material));
            if (for_rigid && found == table->end()) {
                found = table->find(phyxels[index[i]].material);
                for_rigid = false;
            }
            #else
            auto found = table->find(phyxels[index[i]].material);
            #endif
            if (found == table->end()) continue;
            ChemicalReaction* react = &(found->second);
            if (
                #if PHX_ENABLE_TEMPERATURE
                (react->requiredTemperature <= tau_cur) &&
                #endif
                rnd <= react->prob
            ) {
                cur_material = react->newSelf;
                #if PHX_ENABLE_RIGIDBODIES
                if (from_rigid && phyxels[index_cur].coveringRigid && react->newSelf != phyxels[index_cur].coveringRigid->material) { // FIXME: phyxels[index_cur].coveringRigid ?? it might be checked above ONCE
                    phyxels[index_cur].coveringRigid->phyxelate();
                    phyxels[index_cur].coveringRigid->toDelete = true;
                    setMaterial(index_cur, cur_material, rnd);
                    #if PHX_ENABLE_TEMPERATURE
                    phyxels[index_cur].coveringRigid->temperature += react->deltaSelfTemperature;
                    #endif
                } else if (!from_rigid) {
                    setMaterial(index_cur, cur_material, rnd);
                    #if PHX_ENABLE_TEMPERATURE
                    phyxels[index_cur].temperature += react->deltaSelfTemperature;
                    #endif
                }
                if (for_rigid && react->newOther != phyxels[index[i]].coveringRigid->material) {
                    phyxels[index[i]].coveringRigid->phyxelate();
                    phyxels[index[i]].coveringRigid->toDelete = true;
                    setMaterial(index[i], react->newOther, rnd);
                    #if PHX_ENABLE_TEMPERATURE
                    phyxels[index[i]].coveringRigid->temperature += react->deltaOtherTemperature;
                    #endif
                } else if (!for_rigid) {
                    setMaterial(index[i], react->newOther, rnd);
                    #if PHX_ENABLE_TEMPERATURE
                    phyxels[index[i]].temperature += react->deltaOtherTemperature;
                    #endif
                }
                #else
                setMaterial(index_cur, cur_material, rnd);
                setMaterial(index[i], react->newOther, rnd);
                #if PHX_ENABLE_TEMPERATURE
                phyxels[index_cur].temperature += react->deltaSelfTemperature;
                phyxels[index[i]].temperature += react->deltaOtherTemperature;
                #endif
                #endif
                return true;
            }
        }
        #if PHX_ENABLE_RIGIDBODIES
        } // closes the for
        #endif
    }

    return false;
}
#endif // PHX_ENABLE_CHEMISTRY

#if PHX_ENABLE_BURNING
void Scene::updateBurning(size_t X, size_t Y) { // only updates what's burning
    bool res;
    size_t index_cur = sizeX*Y+X;
    auto cur = phyxels + index_cur; // current phyxel that we're looking at
    auto cur_material = cur->material;
    if (cur_material->isFlamable) {
        int rnd = rand();
        PhyxelData* neighbours[] {
            phyxels + index_cur-1,
            phyxels + index_cur+1,
            phyxels + index_cur-sizeX,
            phyxels + index_cur+sizeX,
            phyxels + index_cur-sizeX+1,
            phyxels + index_cur-sizeX-1,
            phyxels + index_cur+sizeX+1,
            phyxels + index_cur+sizeX-1
        };
        switch(cur->burningStage) {
            #if PHX_ENABLE_TEMPERATURE
            case 0: // not burning
            if(cur->temperature >= cur_material->burningTemperature) {
                cur->burningStage = 1;
                cur->burningTimer = 10;
                return;
            }
            break;
            #endif

            case 1: // ready to burn but no oxygen nearby
            res = 1;
            for(uint8_t i=0; i<8; ++i) {
                MaterialObj* nbrMat;
                uint8_t* nbrStage;
                #if PHX_ENABLE_RIGIDBODIES
                if (neighbours[i]->isRigidFree) {
                #endif
                    nbrMat = &neighbours[i]->material;
                    nbrStage = &neighbours[i]->burningStage;
                #if PHX_ENABLE_RIGIDBODIES
                } else {
                    auto* nbrRigid = neighbours[i]->coveringRigid;
                    nbrMat = &nbrRigid->material;
                    nbrStage = &nbrRigid->burningStage;
                }
                #endif
                if((*nbrMat)->containsO2) {
                    cur->burningStage = 2;
                    cur->burningTimer = 10;
                    return;
                }
                if(res&&(*nbrStage)==2) {
                    res = 0;
                    cur->burningTimer = 10;
                }
            }
            if (!res) return;
            cur->burningStage = ((--(cur->burningTimer))!=0);
            break;

            default: // burning
            cur_material->customBurning(this, X, Y); // the visuals are handled here. customBurning is a functor field and can be changed freely
            #if PHX_ENABLE_TEMPERATURE
            cur->temperature += PHX_BURNING_HEAT;
            #endif
            res = (rnd%9) == 0; // res=1 means we haven't found oxygen yet
            for (uint8_t i = 0; i < 8; ++i) {
                MaterialObj* nbrMat;
                uint8_t* nbrStage;
                time_t* nbrTimer;
                #if PHX_ENABLE_RIGIDBODIES
                if (neighbours[i]->isRigidFree) {
                #endif
                    nbrMat = &neighbours[i]->material;
                    nbrStage = &neighbours[i]->burningStage;
                    nbrTimer = &neighbours[i]->burningTimer;
                #if PHX_ENABLE_RIGIDBODIES
                } else {
                    auto* nbrRigid = neighbours[i]->coveringRigid;
                    nbrMat = &nbrRigid->material;
                    nbrStage = &nbrRigid->burningStage;
                    nbrTimer = &nbrRigid->burningTimer;
                }
                #endif

                if ((*nbrMat)->isFlamable && !*nbrStage) {
                    *nbrStage = 1;
                    *nbrTimer = 10;
                }
                if (res && (*nbrMat)->containsO2) {
                    res = 0;
                    cur->burningTimer = 10;
                    #if PHX_ENABLE_RIGIDBODIES
                    if (neighbours[i]->isRigidFree) {
                    #endif
                        setMaterial(neighbours[i]-phyxels, cur_material->burningGas, rnd);
                    #if PHX_ENABLE_RIGIDBODIES
                    } else {
                        neighbours[i]->coveringRigid->phyxelate();
                    }
                    #endif
                }
            }

            if (!res) {
                if (cur->burningStage == cur_material->burningFrames) {
                    cur->burningStage = 0;
                    setMaterial(index_cur, cur_material->burningGas, rnd);
                } else ++(cur->burningStage);
                return;
            }
            if((--(cur->burningTimer))==0) {
                cur->burningStage = 0;
                setMaterial(index_cur, cur_material->burntState, rnd);
            }
            break;
        }
    }
}
#endif // PHX_ENABLE_BURNING

void Scene::updateParticles(void* custom_arguments = NULL) {
    static int8_t randVx[] = {0, 1, 1, 1, 0, -1, -1, -1};
    static int8_t randVy[] = {1, 1, 0, -1, -1, -1, 0, 1};
    MaterialObj tmpMat; Color tmpCol;
    int rnd;
    PhyxelData* nextphyx = NULL;
    for (size_t i=0; i < particles.size(); ++i) {
        auto p = particles[i];

        // DELETE PARTICLE IF NEEDED
        if (p->toDelete) {
            /// delete particle
            p->customDestructor(p);
            delete p;
            particles.erase(particles.begin()+i);
            --i;
            continue;
        }

        // UPDATE LIFETIME
        ++(p->lifetime);

        // CUSTOM UPDATE
        bool stop_this_update = p->customUpdate(p, custom_arguments);
        if (stop_this_update) continue;

        //
        rnd = rand();

        // VIRTUAL PARTICLES (THOSE LOOKING FOR A SPACE TO BE PLACED)
        if (p->isVirtual) {
            auto _X = p->X + p->Vx;
            auto _Y = p->Y + p->Vy;
            PhyxelData* phyx = phyxels + (sizeX*(size_t)_Y+(size_t)_X);
            if (
                #if PHX_ENABLE_RIGIDBODIES
                !(phyx->isRigidFree) ||
                #endif
                p->material->type < phyx->material->type) {
                p->Vx = randVx[rnd&7];
                p->Vy = randVy[rnd&7];
                continue;
            }
            if (
                #if PHX_ENABLE_RIGIDBODIES
                phyx->isRigidFree &&
                #endif
                p->material->type > phyx->material->type ||
                    (p->material->type == phyx->material->type && p->material != phyx->material)) {
                tmpMat = phyx->material;
                tmpCol = phyx->color;
                phyx->material = p->material; // not with setMaterial since the attached box2D body doesn't have to be erased
                phyx->color = p->color;
                #if PHX_ENABLE_BURNING
                phyx->tryToBurn(p->isBurning);
                #endif
                if(tmpMat->isRemovable) {
                    p->toDelete = true;
                    continue;
                }
                p->material = tmpMat;
                p->color = tmpCol;
                p->Vx = randVx[rnd&7];
                p->Vy = randVy[rnd&7];
            }
            p->X = _X; p->Y = _Y;
            continue;
        }
        if(!p->checkCollisions) {
            // POSITION UPDATE
            p->X += p->Vx * particlesTimeStep;
            p->Y += p->Vy * particlesTimeStep;
        } else if (p->material) {
            int n = ceil(std::max(abs(p->Vx), abs(p->Vy)) * particlesTimeStep);
            float dx = p->Vx * particlesTimeStep / n;
            float dy = p->Vy * particlesTimeStep / n;

            for(int _=0; _<n; ++_) {
                // COMPUTE THE NEXT POSITION
                auto _X = p->X + dx;
                auto _Y = p->Y + dy;
                nextphyx = phyxels + (sizeX*(size_t)(_Y)+(size_t)(_X)); // the phyxel located where the particle is going to be

                // CHECKING COLLISIONS
                PhyxelData* thisphyx = phyxels + (sizeX*(size_t)(p->Y)+(size_t)(p->X)); // the phyxel located directly where the particle is
                #if PHX_ENABLE_BURNING
                thisphyx->tryToBurn(p->isBurning);
                #endif

                if (
                    #if PHX_ENABLE_RIGIDBODIES
                    !nextphyx->isRigidFree ||
                    #endif
                    p->material->type <= nextphyx->material->type) {
                    if (
                        #if PHX_ENABLE_RIGIDBODIES
                        !thisphyx->isRigidFree ||
                        #endif
                        p->material->type <= thisphyx->material->type) {
                        p->isVirtual = true;
                        p->Vx = randVx[rnd&7];
                        p->Vy = randVy[rnd&7];
                        stop_this_update = true;
                        break;
                    }
                    if (
                        #if PHX_ENABLE_RIGIDBODIES
                        thisphyx->isRigidFree &&
                        #endif
                        !(thisphyx->material->isRemovable)
                    ) {
                        this->addVirtualParticle( // we pop the removed phyxel out as a virtual particle
                            thisphyx->material,
                            thisphyx->color,
                            (size_t)(p->X), (size_t)(p->Y),
                            1, 1
                        );
                    }
                    thisphyx->material = p->material; // not with setMaterial since the attached box2D body doesn't have to be erased
                    thisphyx->color = p->color;
                    #if PHX_ENABLE_BURNING
                    nextphyx->tryToBurn(p->isBurning);
                    #endif
                    /// delete particle
                    p->toDelete = true;
                    stop_this_update = true;
                    break;
                }

                // POSITION UPDATE
                p->X = _X;
                p->Y = _Y;
            }
        } else {
            int n = ceil(std::max(abs(p->Vx), abs(p->Vy)) * particlesTimeStep);
            float dx = p->Vx * particlesTimeStep / n;
            float dy = p->Vy * particlesTimeStep / n;

            for(int _=0; _<n; ++_) {
                // COMPUTE THE NEXT POSITION
                auto _X = p->X + dx;
                auto _Y = p->Y + dy;
                nextphyx = phyxels + (sizeX*(size_t)(_Y)+(size_t)(_X)); // the phyxel located where the particle is going to be

                // CHECKING COLLISIONS
                PhyxelData* thisphyx = phyxels + (sizeX*(size_t)(p->Y)+(size_t)(p->X)); // the phyxel located directly where the particle is
                #if PHX_ENABLE_BURNING
                thisphyx->tryToBurn(p->isBurning);
                #endif

                if (
                    #if PHX_ENABLE_RIGIDBODIES
                    !nextphyx->isRigidFree ||
                    #endif
                    PHX_MTYPE_LIQ <= nextphyx->material->type) {
                    /// delete particle
                    p->toDelete = true;
                    stop_this_update = true;
                    break;
                }

                // POSITION UPDATE
                p->X = _X;
                p->Y = _Y;
            }
        }

        if(stop_this_update) continue;

        // VELOCITY UPDATE
        if (p->isAffectedByGravity) {
            #if PHX_ENABLE_CUSTOMGRAVITY==0
            p->Vy += particlesTimeStep * PHX_FREEFALL_ACCELERATION;
            #endif
            #if PHX_ENABLE_CUSTOMGRAVITY
            switch(nextphyx->forceDir) {
            // DOWN IS DOWN
                case PHX_FORCE_DOWN:
                p->Vy += particlesTimeStep * PHX_FREEFALL_ACCELERATION;
                break;

            // DOWN IS RIGHT
                case PHX_FORCE_RIGHT:
                p->Vx += particlesTimeStep * PHX_FREEFALL_ACCELERATION;
                break;

            // DOWN IS UP
                case PHX_FORCE_UP:
                p->Vy -= particlesTimeStep * PHX_FREEFALL_ACCELERATION;
                break;

            // DOWN IS LEFT
                case PHX_FORCE_LEFT:
                p->Vx -= particlesTimeStep * PHX_FREEFALL_ACCELERATION;
                break;

            // (1/sqrt(2) approximately equals to 0.7)
            // DOWN IS BETWEEN RIGHT AND DOWN
                case PHX_FORCE_DOWNRIGHT:
                p->Vx += particlesTimeStep * 0.7 * PHX_FREEFALL_ACCELERATION;
                p->Vy += particlesTimeStep * 0.7 * PHX_FREEFALL_ACCELERATION;
                break;
            // DOWN IS BETWEEN RIGHT AND UP
                case PHX_FORCE_UPRIGHT:
                p->Vx += particlesTimeStep * 0.7 * PHX_FREEFALL_ACCELERATION;
                p->Vy -= particlesTimeStep * 0.7 * PHX_FREEFALL_ACCELERATION;
                break;
            // DOWN IS BETWEEN LEFT AND UP
                case PHX_FORCE_UPLEFT:
                p->Vx -= particlesTimeStep * 0.7 * PHX_FREEFALL_ACCELERATION;
                p->Vy -= particlesTimeStep * 0.7 * PHX_FREEFALL_ACCELERATION;
                break;
            // DOWN IS BETWEEN LEFT AND DOWN
                case PHX_FORCE_DOWNLEFT:
                p->Vx -= particlesTimeStep * 0.7 * PHX_FREEFALL_ACCELERATION;
                p->Vy += particlesTimeStep * 0.7 * PHX_FREEFALL_ACCELERATION;
                break;
            }
            #endif
        }
    }
}
#if PHX_ENABLE_RIGIDBODIES
void Scene::rigidBodyFrameLoop(RigidBody* body) {
    size_t newBodyX, newBodyY;
    b2Body* newBody;

    for (auto & coord : body->framePhyxels) {
        newBodyX = coord.first;
        newBodyY = coord.second;
        size_t idx = sizeX*newBodyY+newBodyX;

        if (phyxels[idx].material->type==PHX_MTYPE_SOL || phyxels[idx].material->type==PHX_MTYPE_POD) {
            bool createNewBody = true;
            size_t foundIdx = 0;
            for (size_t i = 0, e = phyxels[idx].box2DTmpBodies.size(); i < e; ++i) {
                auto* phyxBody = phyxels[idx].box2DTmpBodies[i];
                auto* fixData = reinterpret_cast<FixtureData*>(phyxBody->GetFixtureList()->GetUserData().pointer);
                if (reinterpret_cast<b2Fixture*>(fixData->data)==body->box2DBody->GetFixtureList()) {
                    createNewBody = false;
                    foundIdx = i;
                    break;
                }
            }
            if (createNewBody) {
                //box2DStaticBodyDef.position.Set((newBodyX + 0.5) * PHX_PHYXELS2METERS, (newBodyY + 0.5) * PHX_PHYXELS2METERS);
                box2DStaticFixtureDef.userData.pointer = reinterpret_cast<uintptr_t>(new FixtureData(body->box2DBody->GetFixtureList(), 0));
                newBody = box2DLayer->CreateBody(&box2DStaticBodyDef);
                newBody->CreateFixture(&box2DStaticFixtureDef); // what happened to the fixture
                newBody->SetTransform(
                        b2Vec2{static_cast<float>((newBodyX + 0.5) * PHX_PHYXELS2METERS), static_cast<float>((newBodyY + 0.5) * PHX_PHYXELS2METERS)}, 0.f);
                phyxels[idx].box2DTmpBodies.push_back(newBody);
                phyxels[idx].deleteTmpBody.push_back(false);
                continue;
            }
            phyxels[idx].deleteTmpBody[foundIdx] = false;
        }
    }
}

void Scene::updateRigidBodies(void* custom_arguments = NULL) { // only updates rigid bodies
    // bodies step
    box2DLayer->Step(box2DTimeStep, box2DVelocityIterations, box2DPositionIterations);
    for (size_t i = 0; i < rigidBodies.size(); ++i) {
        auto body = rigidBodies[i];

        /// CUSTOM GRAVITY
        b2Vec2 center = PHX_METERS2PHYXELS * body->box2DBody->GetPosition();

        /// DELETE INSTANCE IF NEEDED
        if (body->toDelete) {
            /// delete body
            delete body;
            rigidBodies.erase(rigidBodies.begin()+i);
            --i;
            //box2DLayer->Step(box2DTimeStep/1000., 1, 1);
            continue;
        }

        if (center.x > sizeX || center.y > sizeY) {
            // When we get to chunks, modify this to sort out the a management orf a a so when we get to chunks vsi podumayu
            // (write the body to disk)
            body->toDelete = true;
            continue;
        }

        #if PHX_ENABLE_CUSTOMGRAVITY
        auto center_phyx = phyxels + sizeX * (size_t)center.y + (size_t)center.x;
        double gravity = body->gravForce;
        double gravProj; // projection of gravitational force (takes into account Archimedes' force)

        switch(center_phyx->forceDir) {
        // DOWN IS DOWN
            case PHX_FORCE_DOWN:
            body->box2DBody->ApplyForce(
                b2Vec2(0, - gravity * body->backgroundMaterialMass
                     * body->material->invMass),
                body->box2DBody->GetWorldCenter(), true);
            break;

        // DOWN IS RIGHT
            case PHX_FORCE_RIGHT:
            gravProj = gravity * (1 - body->backgroundMaterialMass
                 * body->material->invMass);
            body->box2DBody->ApplyForce(
                b2Vec2(gravProj, -gravity),
                body->box2DBody->GetWorldCenter(), true);
            break;

        // DOWN IS UP
            case PHX_FORCE_UP:
            gravProj = gravity * (1 - body->backgroundMaterialMass
                * body->material->invMass);
            body->box2DBody->ApplyForce(
                b2Vec2(0.0f, - gravProj - gravity),
                body->box2DBody->GetWorldCenter(), true);
            break;

        // DOWN IS LEFT
            case PHX_FORCE_LEFT:
            gravProj = gravity * (1 - body->backgroundMaterialMass
                * body->material->invMass);
            body->box2DBody->ApplyForce(
                b2Vec2(-gravProj, -gravity),
                body->box2DBody->GetWorldCenter(), true);
            break;

        // (1/sqrt(2) approximately equals to 0.7)
        // DOWN IS BETWEEN RIGHT AND DOWN
            case PHX_FORCE_DOWNRIGHT:
            gravProj = 0.7 * gravity * (1 - body->backgroundMaterialMass
                * body->material->invMass);
            body->box2DBody->ApplyForce(
                b2Vec2(gravProj, gravProj - gravity),
                body->box2DBody->GetWorldCenter(), true);
            break;
        // DOWN IS BETWEEN RIGHT AND UP
            case PHX_FORCE_UPRIGHT:
            gravProj = 0.7 * gravity * (1 - body->backgroundMaterialMass
                * body->material->invMass);
            body->box2DBody->ApplyForce(
                b2Vec2(gravProj, - gravProj - gravity),
                body->box2DBody->GetWorldCenter(), true);
            break;
        // DOWN IS BETWEEN LEFT AND UP
            case PHX_FORCE_UPLEFT:
            gravProj = 0.7 * gravity * (1 - body->backgroundMaterialMass
                * body->material->invMass);
            body->box2DBody->ApplyForce(
                b2Vec2(-gravProj, - gravProj - gravity),
                body->box2DBody->GetWorldCenter(), true);
            break;
        // DOWN IS BETWEEN LEFT AND DOWN
            case PHX_FORCE_DOWNLEFT:
            gravProj = 0.7 * gravity * (1 - body->backgroundMaterialMass
                * body->material->invMass);
            body->box2DBody->ApplyForce(
                b2Vec2(-gravProj, gravProj - gravity),
                body->box2DBody->GetWorldCenter(), true);
            break;
        }
        #endif

        #if PHX_ENABLE_CUSTOMGRAVITY==0
        center = PHX_METERS2PHYXELS * body->box2DBody->GetWorldCenter();
        auto& center_phyx = phyxels[sizeX * (size_t)center.y + (size_t)center.x];
        double gravity = PHX_BOX2D_FREEFALL_ACCELERATION*body->box2DBody->GetMass();
        body->box2DBody->ApplyForce(
            b2Vec2(0, - gravity * body->backgroundMaterialMass
                 * body->material->invMass),
            body->box2DBody->GetWorldCenter(), true);
        #endif

        // Slow down the object due to the medium's viscosity
        body->box2DBody->ApplyForce(
            - 0.3 * body->material->mass * body->box2DBody->GetLinearVelocity(),
            body->box2DBody->GetWorldCenter(), true);

        /// CLEAR THE COVERED PHYXELS
        for (const auto& c : body->coveredPhyxels) {
            size_t idx = sizeX*c.second + c.first;
            phyxels[idx].isRigidFree = true;
            phyxels[idx].coveringRigid = NULL;
        }

        rigidBodyFrameLoop(body);

        /// UPDATE LIFETIME
        ++(body->lifetime);

        // CUSTOM UPDATE
        body->customUpdate(body, custom_arguments);

        // UPDATE INNER POSITION DETAILS
        body->recalculate();
    }
}

void Scene::cleanupTmpBodies(size_t X, size_t Y) {
    auto* phyx = phyxels + sizeX * Y + X;
    auto& bodies = phyx->box2DTmpBodies;
    auto& delBody = phyx->deleteTmpBody;
    for (size_t i = 1, e = bodies.size()+1; i < e; ++i) {
        size_t n = i - 1;
        if (delBody[n]) {
            auto* fixData = reinterpret_cast<FixtureData*>(bodies[n]->GetFixtureList()->GetUserData().pointer);
            delete fixData;
            box2DLayer->DestroyBody(bodies[n]);
            bodies.erase(bodies.begin()+n);
            delBody.erase(delBody.begin()+n);
            --i;
            --e;
            continue;
        }
        delBody[n] = true;
    }
}
#endif // PHX_ENABLE_RIGIDBODIES

void Scene::updateInstances(void* custom_arguments = NULL) { // only updates other instances (not including rigid bodies)
    for (size_t i = 0; i < instances.size(); ++i) {
        auto inst = instances[i];

        // DELETE INSTANCE IF NEEDED
        if (inst->toDelete) {
            /// delete instance
            delete inst;
            instances.erase(instances.begin()+i);
            --i;
            continue;
        }

        // UPDATE LIFETIME
        ++(inst->lifetime);

        // CUSTOM UPDATE
        inst->customUpdate(inst, custom_arguments);
    }
}

bool Scene::updatePhyxelMovement(size_t X, size_t Y, bool loopDir) { // updates everything except for temperature
    int8_t dir, binrand;
    int rnd;
    size_t X_up, Y_up, X_down, Y_down, X_plus, Y_plus, X_minus, Y_minus, X_down_plus, Y_down_plus, X_down_minus, Y_down_minus, index_up_minus, index_up_plus;
    MaterialObj up = NULL, up_plus = NULL, up_minus = NULL;
    size_t index_cur = sizeX*Y+X;

    auto cur = phyxels[index_cur].material; // current phyxel that we're looking at
    binrand = rand() % 2;

    if (cur->isPassive) {
        phyxels[index_cur].lockMask = 0;
        return false;
    }
    if (phyxels[index_cur].lockMask>>1) {
        phyxels[index_cur].lockMask-=2;
        return false;
    }

    dir = binrand * 2 - 1; // randomly pick a direction to check first
    rnd = rand() & 8191;

    /// GRAVITY
    #if PHX_ENABLE_CUSTOMGRAVITY
    bool isVertical = true; // tells whether the force field is vertically aligned. this is used in our new liquids' mechanics
    switch (phyxels[index_cur].forceDir) {
    // DOWN IS DOWN
        case PHX_FORCE_DOWN:
        X_up = X; Y_up = Y-1;
        X_down = X; Y_down = Y + 1;
        X_plus = X + dir; Y_plus = Y;
        X_minus = X - dir; Y_minus = Y;
        X_down_plus = X + dir; Y_down_plus = Y + 1;
        X_down_minus = X - dir; Y_down_minus = Y + 1;

        up = phyxels[sizeX*Y_up+X].material;
        up_plus = phyxels[sizeX*Y_up+X_plus].material;
        up_minus = phyxels[sizeX*Y_up+X_minus].material;
        break;

    // DOWN IS RIGHT
        case PHX_FORCE_RIGHT:
        isVertical = false;
        X_up = X - 1; Y_up = Y;
        X_down = X + 1; Y_down = Y;
        X_plus = X; Y_plus = Y + dir;
        X_minus = X; Y_minus = Y - dir;
        X_down_plus = X + 1; Y_down_plus = Y + dir;
        X_down_minus = X + 1; Y_down_minus = Y - dir;

        up = phyxels[sizeX*Y+(X-1)].material;
        up_plus = phyxels[sizeX*(Y+dir)+(X-1)].material;
        up_minus = phyxels[sizeX*(Y-dir)+(X-1)].material;
        break;

    // DOWN IS UP
    case PHX_FORCE_UP:
        X_up = X; Y_up = Y + 1;
        X_down = X; Y_down = Y - 1;
        X_plus = X + dir; Y_plus = Y;
        X_minus = X - dir; Y_minus = Y;
        X_down_plus = X + dir; Y_down_plus = Y - 1;
        X_down_minus = X - dir; Y_down_minus = Y - 1;

        up = phyxels[sizeX*(Y+1)+X].material;
        up_plus = phyxels[sizeX*(Y+1)+(X+dir)].material;
        up_minus = phyxels[sizeX*(Y+1)+(X-dir)].material;
        break;

    // DOWN IS LEFT
    case PHX_FORCE_LEFT:
        isVertical = false;
        X_up = X + 1; Y_up = Y;
        X_down = X - 1; Y_down = Y;
        X_plus = X; Y_plus = Y + dir;
        X_minus = X; Y_minus = Y - dir;
        X_down_plus = X - 1; Y_down_plus = Y + dir;
        X_down_minus = X - 1; Y_down_minus = Y - dir;

        up = phyxels[sizeX*Y+(X+1)].material;
        up_plus = phyxels[sizeX*(Y+dir)+(X+1)].material;
        up_minus = phyxels[sizeX*(Y-dir)+(X+1)].material;
        break;

    // DOWN IS BETWEEN RIGHT AND DOWN
    case PHX_FORCE_DOWNRIGHT:
        isVertical = false;
        X_up = X - 1; Y_up = Y - 1;
        X_down = X + 1; Y_down = Y + 1;
        X_plus = X - dir; Y_plus = Y + dir;
        X_minus = X + dir; Y_minus = Y - dir;
        X_down_plus = X + (dir==-1); Y_down_plus = Y + (dir==1);
        X_down_minus = X + (dir==1); Y_down_minus = Y + (dir==-1);

        up = phyxels[sizeX*(Y-1)+(X-1)].material;
        up_plus = phyxels[sizeX*(Y - (dir==1))+(X - (dir==-1))].material;
        up_minus = phyxels[sizeX*(Y - (dir==-1))+(X - (dir==1))].material;
        break;
    // DOWN IS BETWEEN RIGHT AND UP
    case PHX_FORCE_UPRIGHT:
        isVertical = false;
        X_up = X - 1; Y_up = Y + 1;
        X_down = X + 1; Y_down = Y - 1;
        X_plus = X + dir; Y_plus = Y + dir;
        X_minus = X - dir; Y_minus = Y - dir;
        X_down_plus = X + (dir==1); Y_down_plus = Y - (dir==-1);
        X_down_minus = X + (dir==-1); Y_down_minus = Y - (dir==1);

        up = phyxels[sizeX*(Y+1)+(X-1)].material;
        up_plus = phyxels[sizeX*(Y + (dir==1))+(X - (dir==-1))].material;
        up_minus = phyxels[sizeX*(Y + (dir==-1))+(X - (dir==1))].material;
        break;
    // DOWN IS BETWEEN LEFT AND UP
    case PHX_FORCE_UPLEFT:
        isVertical = false;
        X_up = X + 1; Y_up = Y + 1;
        X_down = X - 1; Y_down = Y - 1;
        X_plus = X - dir; Y_plus = Y + dir;
        X_minus = X + dir; Y_minus = Y - dir;
        X_down_plus = X - (dir==1); Y_down_plus = Y - (dir==-1);
        X_down_minus = X - (dir==-1); Y_down_minus = Y - (dir==1);

        up = phyxels[sizeX*(Y+1)+(X+1)].material;
        up_plus = phyxels[sizeX*(Y + (dir==1))+(X + (dir==-1))].material;
        up_minus = phyxels[sizeX*(Y + (dir==-1))+(X + (dir==1))].material;
        break;
    // DOWN IS BETWEEN LEFT AND DOWN
    case PHX_FORCE_DOWNLEFT:
        isVertical = false;
        X_up = X + 1; Y_up = Y - 1;
        X_down = X - 1; Y_down = Y + 1;
        X_plus = X + dir; Y_plus = Y + dir;
        X_minus = X - dir; Y_minus = Y - dir;
        X_down_plus = X - (dir==-1); Y_down_plus = Y + (dir==1);
        X_down_minus = X - (dir==1); Y_down_minus = Y + (dir==-1);

        up = phyxels[sizeX*(Y-1)+(X+1)].material;
        up_plus = phyxels[sizeX*(Y - (dir==-1))+(X + (dir==1))].material;
        up_minus = phyxels[sizeX*(Y - (dir==1))+(X + (dir==-1))].material;
        break;
    }
    #endif
    #if PHX_ENABLE_CUSTOMGRAVITY==0
    X_up = X; Y_up = Y-1;
    X_down = X; Y_down = Y + 1;
    X_plus = X + dir; Y_plus = Y;
    X_minus = X - dir; Y_minus = Y;
    X_down_plus = X + dir; Y_down_plus = Y + 1;
    X_down_minus = X - dir; Y_down_minus = Y + 1;

    up = phyxels[sizeX*Y_up+X].material;
    up_plus = phyxels[sizeX*Y_up+X_plus].material;
    up_minus = phyxels[sizeX*Y_up+X_minus].material;
    #endif
    /// END GRAVITY

    auto cur_plus = phyxels[sizeX*Y_plus+X_plus].material; // 1 phyxel in the direcion of dir
    auto cur_minus = phyxels[sizeX*Y_minus+X_minus].material; // 1 phyxel in the direcion opposite to dir
    auto down = phyxels[sizeX*Y_down+X_down].material; // 1 phyxel lower from cur

    /// MOVEMENT
    auto down_plus = phyxels[sizeX*Y_down_plus+X_down_plus].material; // 1 phyxel lower and 1 in the direcion of dir
    auto down_minus = phyxels[sizeX*Y_down_minus+X_down_minus].material; // 1 phyxel lower and 1 in the direcion opposite to dir

    // GASES
    if (cur->type == PHX_MTYPE_GAS) {
        rnd = rand() & 1023;
        phyxels[index_cur].lockMask = 0;
        if (
            #if PHX_ENABLE_RIGIDBODIES
            phyxels[sizeX*Y_up+X_up].isRigidFree &&
            #endif
        up->isPassive && up->type==PHX_MTYPE_GAS && (up->mass != cur->mass || rnd&2) && verticalGasDiffusion <= rnd * (minDeltaMass + up->mass - cur->mass)) {
            swap(X_up, Y_up, X, Y, loopDir);
            return true;
        }
        if (
            #if PHX_ENABLE_RIGIDBODIES
            phyxels[sizeX*Y_down+X_down].isRigidFree &&
            #endif
        down->type==PHX_MTYPE_GAS && verticalGasDiffusion <= rnd * (minDeltaMass + cur->mass - down->mass)) {
            swap(X, Y, X_down, Y_down, loopDir);
            return true;
        }

        if (horizontalGasDiffusion <= rnd) {
            if (
                #if PHX_ENABLE_RIGIDBODIES
                phyxels[sizeX*Y_plus+X_plus].isRigidFree &&
                #endif
            cur_plus->type==PHX_MTYPE_GAS) {
                swapNoLock(X, Y, X_plus, Y_plus, loopDir);
                return true;
            } else if (
                #if PHX_ENABLE_RIGIDBODIES
                phyxels[sizeX*Y_minus+X_minus].isRigidFree &&
                #endif
            cur_minus->type==PHX_MTYPE_GAS) {
                swapNoLock(X, Y, X_minus, Y_minus, loopDir);
                return true;
            }
        }
        return true;
    }
    // END GASES

    // POWDERS
    if (cur->type == PHX_MTYPE_POD) {
        if (
            #if PHX_ENABLE_RIGIDBODIES
            phyxels[sizeX*Y_down+X_down].isRigidFree &&
            #endif
        down->type==PHX_MTYPE_GAS || (down->type==PHX_MTYPE_LIQ && down->mass <= cur->mass)) {
            swap(X, Y, X_down, Y_down, loopDir);
            return true;
        }
        if (
            #if PHX_ENABLE_RIGIDBODIES
            phyxels[sizeX*Y_plus+X_plus].isRigidFree &&
            #endif
        cur_plus->type!=PHX_MTYPE_POD && (
            #if PHX_ENABLE_RIGIDBODIES
            phyxels[sizeX*Y_down_plus+X_down_plus].isRigidFree &&
            #endif
        down_plus->type==PHX_MTYPE_GAS || (down_plus->type==PHX_MTYPE_LIQ && down_plus->mass <= cur->mass))) {
            swap(X, Y, X_down_plus, Y_down_plus, loopDir);
            return true;
        }
        if (
            #if PHX_ENABLE_RIGIDBODIES
            phyxels[sizeX*Y_minus+X_minus].isRigidFree &&
            #endif
        cur_minus->type!=PHX_MTYPE_POD && (
            #if PHX_ENABLE_RIGIDBODIES
            phyxels[sizeX*Y_down_minus+X_down_minus].isRigidFree &&
            #endif
        down_minus->type==PHX_MTYPE_GAS || (down_minus->type==PHX_MTYPE_LIQ && down_minus->mass <= cur->mass))) {
            swap(X, Y, X_down_minus, Y_down_minus, loopDir);
            return true;
        }
        phyxels[index_cur].lockMask = 0;
        return true;
    }
    // END POWDERS

    // LIQUIDS
    if (cur->type == PHX_MTYPE_LIQ) {
        size_t index_plus = sizeX*Y_plus+X_plus;
        size_t index_minus = sizeX*Y_minus+X_minus;
        rnd = rand()&8191;
        // falling
        #if PHX_ENABLE_RIGIDBODIES
        if (phyxels[sizeX*Y_down+X_down].isRigidFree)
        #endif
        if (down->type==PHX_MTYPE_GAS || (down->type==PHX_MTYPE_LIQ && down->mass < cur->mass)
        ||(down->type==PHX_MTYPE_POD && down->mass <= cur->mass)) {
            swap(X, Y, X_down, Y_down, loopDir);
            return true;
        }
        if(rnd < cur->viscosity) return false;
        if(horizontalGasDiffusion <= rnd) {
            if(
            #if PHX_ENABLE_RIGIDBODIES
            phyxels[index_plus].isRigidFree &&
            #endif
            cur_plus->type==PHX_MTYPE_LIQ && cur_plus!=cur && cur_plus->mass <= cur->mass) {
                swapNoLock(index_cur, index_plus, loopDir);
                return true;
            }
            if(
            #if PHX_ENABLE_RIGIDBODIES
            phyxels[index_minus].isRigidFree &&
            #endif
            cur_minus->type==PHX_MTYPE_LIQ && cur_minus!=cur && cur_minus->mass <= cur->mass) {
                swapNoLock(index_cur, index_minus, loopDir);
                return true;
            }
        }
        // spreading
        #if PHX_ENABLE_CUSTOMGRAVITY
        if (isVertical) {
        #endif
        // spreading 2.0 (for vertical gravity only), also works when no custom gravity/force fields are enabled
        rnd &= 1023;
        size_t travelLimit = inv_log_normal_CDF(rnd); // the travel limit is generated according to a log-normal distribution
        if (!travelLimit) return false;
        size_t XP = X+dir, XM = X-dir;
        size_t X_max = X+travelLimit*dir, X_min = X-travelLimit*dir;
        size_t index_down_plus = sizeX*Y_down+XP;
        size_t index_down_minus = sizeX*Y_down+XM;

        while (XP!=X_max) {
            #if PHX_ENABLE_RIGIDBODIES
            if (phyxels[index_down_plus].isRigidFree)
            #endif
            if (phyxels[index_down_plus].material->type==PHX_MTYPE_GAS ||
            (phyxels[index_down_plus].material->type==PHX_MTYPE_POD &&
            phyxels[index_down_plus].material->mass <= cur->mass)) {
                swap(X, Y, XP, Y_down, loopDir);
                phyxels[index_cur].lockMask = 0;
                --phyxels[index_down_plus].lockMask;
                return true;
            }
            if (
                #if PHX_ENABLE_RIGIDBODIES
                !phyxels[index_down_plus].isRigidFree ||
                !phyxels[index_plus].isRigidFree ||
                #endif
                phyxels[index_down_plus].material->type!=PHX_MTYPE_LIQ ||
                phyxels[index_plus].material->type!=PHX_MTYPE_GAS
            ) {
                while ((
                #if PHX_ENABLE_RIGIDBODIES
                !(phyxels[index_down_minus].isRigidFree) ||
                #endif
                phyxels[index_down_minus].material->type!=PHX_MTYPE_LIQ) &&
                (
                #if PHX_ENABLE_RIGIDBODIES
                (phyxels[index_minus].isRigidFree) &&
                #endif
                phyxels[index_minus].material->type==PHX_MTYPE_GAS) && XM!=X_min) {
                    XM-=dir;
                    index_down_minus-=dir;
                    index_minus-=dir;
                }
                #if PHX_ENABLE_RIGIDBODIES
                if (phyxels[index_down_minus].isRigidFree)
                #endif
                if (phyxels[index_down_minus].material->type==PHX_MTYPE_GAS ||
                (phyxels[index_down_minus].material->type==PHX_MTYPE_POD &&
                phyxels[index_down_minus].material->mass <= cur->mass)) {
                    swap(X, Y, XM, Y_down, loopDir);
                    phyxels[index_cur].lockMask = 0;
                    --phyxels[index_down_minus].lockMask;
                    return true;
                }
                #if PHX_ENABLE_RIGIDBODIES
                if (phyxels[index_minus].isRigidFree)
                #endif
                if (phyxels[index_minus].material->type==PHX_MTYPE_GAS ||
                (phyxels[index_minus].material->type==PHX_MTYPE_POD &&
                phyxels[index_minus].material->mass <= cur->mass)) {
                    swapNoLock(index_cur, index_minus, loopDir);
                    phyxels[index_cur].lockMask = 0;
                    return true;
                }
                index_minus += dir;
                #if PHX_ENABLE_RIGIDBODIES
                if (phyxels[index_minus].isRigidFree)
                #endif
                swapNoLock(index_cur, index_minus, loopDir);
                phyxels[index_cur].lockMask = 0;
                return true;
            }
            #if PHX_ENABLE_RIGIDBODIES
            if (phyxels[index_down_minus].isRigidFree)
            #endif
            if (phyxels[index_down_minus].material->type==PHX_MTYPE_GAS ||
            (phyxels[index_down_minus].material->type==PHX_MTYPE_POD &&
            phyxels[index_down_minus].material->mass <= cur->mass)) {
                swap(X, Y, XM, Y_down, loopDir);
                phyxels[index_cur].lockMask = 0;
                --phyxels[index_down_minus].lockMask;
                return true;
            }
            if (
            #if PHX_ENABLE_RIGIDBODIES
            !(phyxels[index_down_minus].isRigidFree) || !(phyxels[index_minus].isRigidFree) ||
            #endif
            phyxels[index_down_minus].material->type!=PHX_MTYPE_LIQ ||
            phyxels[index_minus].material->type!=PHX_MTYPE_GAS) {
                XP+=dir;
                index_down_plus+=dir;
                index_plus+=dir;
                while ((
                #if PHX_ENABLE_RIGIDBODIES
                (phyxels[index_down_plus].isRigidFree) &&
                #endif
                phyxels[index_down_plus].material==cur) &&
                (
                #if PHX_ENABLE_RIGIDBODIES
                (phyxels[index_plus].isRigidFree) &&
                #endif
                phyxels[index_plus].material->type==PHX_MTYPE_GAS) && XP!=X_max) {
                    XP+=dir;
                    index_down_plus+=dir;
                    index_plus+=dir;
                }
                #if PHX_ENABLE_RIGIDBODIES
                if (phyxels[index_down_plus].isRigidFree)
                #endif
                if (phyxels[index_down_plus].material->type==PHX_MTYPE_GAS ||
                (phyxels[index_down_plus].material->type==PHX_MTYPE_POD &&
                phyxels[index_down_plus].material->mass <= cur->mass)) {
                    swap(X, Y, XP, Y_down, loopDir);
                    phyxels[index_cur].lockMask = 0;
                    --phyxels[index_down_plus].lockMask;
                    return true;
                }
                #if PHX_ENABLE_RIGIDBODIES
                if (phyxels[index_plus].isRigidFree)
                #endif
                if (phyxels[index_plus].material->type==PHX_MTYPE_GAS ||
                (phyxels[index_plus].material->type==PHX_MTYPE_POD &&
                phyxels[index_plus].material->mass <= cur->mass)) {
                    swapNoLock(index_cur, index_plus, loopDir);
                    phyxels[index_cur].lockMask = 0;
                    return true;
                }
                index_plus -= dir;
                #if PHX_ENABLE_RIGIDBODIES
                if (phyxels[index_plus].isRigidFree)
                #endif
                swapNoLock(index_cur, index_plus, loopDir);
                phyxels[index_cur].lockMask = 0;
                return true;
            }
            XP+=dir;
            index_plus+=dir;
            index_down_plus+=dir;
            XM-=dir;
            index_down_minus-=dir;
            index_minus-=dir;
        }
        #if PHX_ENABLE_RIGIDBODIES
        if (phyxels[index_down_plus].isRigidFree)
        #endif
        if (phyxels[index_down_plus].material->type==PHX_MTYPE_GAS ||
        (phyxels[index_down_plus].material->type==PHX_MTYPE_POD &&
        phyxels[index_down_plus].material->mass <= cur->mass)) {
            swap(X, Y, XP, Y_down, loopDir);
            phyxels[index_cur].lockMask = 0;
            --phyxels[index_down_plus].lockMask;
            return true;
        }
        #if PHX_ENABLE_RIGIDBODIES
        if (phyxels[index_down_minus].isRigidFree)
        #endif
        if (phyxels[index_down_minus].material->type==PHX_MTYPE_GAS ||
        (phyxels[index_down_minus].material->type==PHX_MTYPE_POD &&
        phyxels[index_down_minus].material->mass <= cur->mass)) {
            swap(X, Y, XM, Y_down, loopDir);
            phyxels[index_cur].lockMask = 0;
            --phyxels[index_down_minus].lockMask;
            return true;
        }
        #if PHX_ENABLE_RIGIDBODIES
        if (phyxels[index_plus].isRigidFree)
        #endif
        if (phyxels[index_plus].material->type==PHX_MTYPE_GAS ||
        (phyxels[index_plus].material->type==PHX_MTYPE_POD &&
        phyxels[index_plus].material->mass <= cur->mass)) {
            swapNoLock(index_cur, index_plus, loopDir);
            phyxels[index_cur].lockMask = 0;
            return true;
        }
        #if PHX_ENABLE_RIGIDBODIES
        if (phyxels[index_minus].isRigidFree)
        #endif
        if (phyxels[index_minus].material->type==PHX_MTYPE_GAS ||
        (phyxels[index_minus].material->type==PHX_MTYPE_POD &&
        phyxels[index_minus].material->mass <= cur->mass)) {
            swapNoLock(index_cur, index_minus, loopDir);
            phyxels[index_cur].lockMask = 0;
            return true;
        }
        index_plus -= dir;
        #if PHX_ENABLE_RIGIDBODIES
        if (phyxels[index_plus].isRigidFree)
        #endif
        swapNoLock(index_cur, index_plus, loopDir);
        phyxels[index_cur].lockMask = 0;
        return true;
        #if PHX_ENABLE_CUSTOMGRAVITY
        // old speading mechanics, still used for non-vertically aligned force fields
        } else { // closes the if (is Vertical) {
            if (cur_plus->type!=PHX_MTYPE_LIQ &&
                #if PHX_ENABLE_RIGIDBODIES
                (phyxels[sizeX*X_down_plus+Y_down_plus].isRigidFree) &&
                #endif
                (down_plus->type==PHX_MTYPE_GAS || (down_plus->type==PHX_MTYPE_POD && down_plus->mass <= cur->mass))) {
                swap(X, Y, X_down_plus, Y_down_plus, loopDir);
                return true;
            }
            if (cur_plus->type!=PHX_MTYPE_LIQ &&
                #if PHX_ENABLE_RIGIDBODIES
                (phyxels[sizeX*X_down_minus+Y_down_minus].isRigidFree) &&
                #endif
                (down_minus->type==PHX_MTYPE_GAS || (down_minus->type==PHX_MTYPE_POD && down_minus->mass <= cur->mass))) {
                swap(X, Y, X_down_minus, Y_down_minus, loopDir);
                return true;
            }
            if (
                #if PHX_ENABLE_RIGIDBODIES
                (phyxels[sizeX*X_down+Y_down].isRigidFree) &&
                #endif
                down->type==PHX_MTYPE_LIQ) {
                if (cur->mass > down->mass && verticalLiqDiffusion <= rnd * (cur->mass - down->mass)) {
                    swap(X, Y, X_down, Y_down, loopDir);
                    return true;
                }
            }
            if (up_plus->type!=PHX_MTYPE_LIQ &&
                #if PHX_ENABLE_RIGIDBODIES
                (phyxels[sizeX*X_plus+Y_plus].isRigidFree) &&
                #endif
                (cur_plus->type==PHX_MTYPE_GAS || (cur_plus->type==PHX_MTYPE_POD && cur_plus->mass <= cur->mass))) {
                swapNoLock(index_cur, index_plus, loopDir);
                phyxels[index_cur].lockMask = 0;
                return true;
            }
            if (up_minus->type!=PHX_MTYPE_LIQ &&
                #if PHX_ENABLE_RIGIDBODIES
                (phyxels[sizeX*X_minus+Y_minus].isRigidFree) &&
                #endif
                (cur_minus->type==PHX_MTYPE_GAS || (cur_minus->type==PHX_MTYPE_POD && cur_minus->mass <= cur->mass))) {
                swapNoLock(index_cur, index_minus, loopDir);
                phyxels[index_cur].lockMask = 0;
                return true;
            }

            if (verticalLiqDiffusion <= rnd) {
                if (
                    #if PHX_ENABLE_RIGIDBODIES
                    (phyxels[sizeX*X_plus+Y_plus].isRigidFree) &&
                    #endif
                    cur_plus->type==PHX_MTYPE_LIQ) {
                    swapNoLock(index_cur, index_plus, loopDir);
                    phyxels[index_cur].lockMask = 0;
                } else if (
                    #if PHX_ENABLE_RIGIDBODIES
                    (phyxels[sizeX*X_minus+Y_minus].isRigidFree) &&
                    #endif
                    cur_minus->type==PHX_MTYPE_LIQ) {
                    swapNoLock(index_cur, index_minus, loopDir);
                    phyxels[index_cur].lockMask = 0;
                }
                return true;
            }
            phyxels[index_cur].lockMask = 0;
            return true;
        }
        #endif
    }
    // END LIQUIDS
    /// END MOVEMENT
    return false;
}

void Scene::updateAllPhyxels() { // combined (temperature + chemistry + burning + electricity + movement) & balanced (bidirectional loop) update for every phyxel in the scene
    #if PHX_ENABLE_RIGIDBODIES
    for (size_t X = 0; X < sizeX; ++X) {
        for (size_t Y = 0; Y < sizeY; ++Y) {
            cleanupTmpBodies(X, Y);
        }
    }
    #endif
    for (size_t Y = sizeY-2; Y > 0; --Y)
        for (size_t X = sizeX-2; X > 0; --X) {
            phyxels[sizeX*Y+X].material->customUpdate(this, X, Y, false); // custom update step
            #if PHX_ENABLE_TEMPERATURE
            updateTemperature(X, Y);
            #endif
            #if PHX_ENABLE_ELECTRICITY
            updateElectricity(X, Y);
            #endif
            #if PHX_ENABLE_CHEMISTRY
            updateChemistry(X, Y);
            #endif
            #if PHX_ENABLE_BURNING
            updateBurning(X, Y);
            #endif
            updatePhyxelMovement(X, Y, false);
        }
    for (size_t Y = 1; Y < sizeY-1; ++Y)
        for (size_t X = 1; X < sizeX-1; ++X) {
            phyxels[sizeX*Y+X].material->customUpdate(this, X, Y, true); // custom update step
            #if PHX_ENABLE_TEMPERATURE
            updateTemperature(X, Y);
            #endif
            #if PHX_ENABLE_ELECTRICITY
            updateElectricity(X, Y);
            #endif
            #if PHX_ENABLE_CHEMISTRY
            updateChemistry(X, Y);
            #endif
            #if PHX_ENABLE_BURNING
            updateBurning(X, Y);
            #endif
            updatePhyxelMovement(X, Y);
        }
}

void Scene::updateAll(
    void* custom_arguments_particles= NULL,
    void* custom_arguments_instances= NULL
    #if PHX_ENABLE_RIGIDBODIES
    ,void* custom_arguments_rigids= NULL
    #endif
) {
    updateAllPhyxels();
    updateParticles(custom_arguments_particles);
    updateInstances(custom_arguments_instances);
    #if PHX_ENABLE_RIGIDBODIES
    updateRigidBodies(custom_arguments_rigids);
    #endif
}

}
