// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.0
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// declare Entity class

#pragma once
#include "phyxel_config.h"

namespace phx {
class Scene; // defined in Scene.hpp

/// Phyxel entities are: Particles, Instances, RigidBodies
class Entity {
    Entity() = delete;
    Entity(const Entity& e); // defined in "EntityUtils.hpp"
    Entity(Entity&& e) noexcept; // defined in "EntityUtils.hpp"
    Entity& operator=(const Entity& e); // defined in "EntityUtils.hpp"
    Entity& operator=(Entity&& e) noexcept; // defined in "EntityUtils.hpp"

    static size_t nextID;
    size_t id;
protected:
    // scene pointer
    Scene* scene;
public:
    bool toDelete = false; // do NOT explicitly delete entities from the scene, just set this field to true instead. deallocate the necessary customFields manually before that
    time_t lifetime = 0; // the number of update steps the entity lived through

    Entity(Scene* scene); // defined in "EntityUtils.hpp"
    virtual ~Entity(); // defined in "EntityUtils.hpp"
    size_t getID() const {
        return id;
    }
    Scene* getScene() const {
        return scene;
    }
};

size_t Entity::nextID = 0;

};