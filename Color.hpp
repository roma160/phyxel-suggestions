// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.0
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// it is expected that you use some external graphic library to visualise what phyxel does. the Phyxel Engine itself does not provide any visualisation methods to be as general as possible and leaves that up to your preference. this struct acts as a placeholder for the colour type from the graphic library of your choice in places where there needs to be some colour. this type is easy to convert into any other possible colour data type that you would use.

#pragma once

namespace phx {

struct Color {
    uint8_t r, g, b, a;

    Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a=255):
        r(r), g(g), b(b), a(a)
    {}
    Color(): Color(0,0,0,0) {}

    friend bool operator<(const Color& c1, const Color& c2) noexcept { // not used in Phyxel Engine, but sometimes useful
        return (c1.r < c2.r) || (c1.r == c2.r && (c1.g < c2.g || (c1.g == c2.g && (c1.b < c2.b || (c1.b == c2.b && c1.a < c2.a)))));
        // return *((uint32_t*)(&c1)) < *((uint32_t*)(&c2));
    }
};

}